<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');

// Use the REST API Client to make requests to the Twilio REST API
//use Twilio\Rest\Client;

class Auth extends REST_Controller
{
	public $empty_object;
	function __construct(){
		parent::__construct();
		$this->load->helper('jwt_helper');
		$this->load->model('app_api/auth_model', 'auth_model');
		$this->load->model('app_api/users_model', 'users_model');
		$this->load->model('app_api/companies_model', 'companies_model');
		$this->load->helper('email_helper');
		$this->empty_object = json_decode('{}');	
	}
	
	public function user_register_post()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[jp_users.email]|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
		
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		//$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
		
		$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
		$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
		
		$this->form_validation->set_rules('device_token', 'Device Token', 'trim|required');
		$this->form_validation->set_rules('device_name', 'Device Name', 'trim|required');
		
		$this->form_validation->set_rules('user_app_language', 'APP Language', 'trim|required');
				
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			$middle_name=$this->input->post('middle_name');
			
			$email=$this->input->post('email');
							
			$password=$this->input->post('password');
						
			$country=$this->input->post('country');
			$state=$this->input->post('state');			
			$city=$this->input->post('city');
			
			$autocomplete=$this->input->post('address');
			$postal_code=$this->input->post('postal_code');
			
			$latitude = $this->input->post('latitude');
			$longitude = $this->input->post('longitude');
			
			$mobile=$this->input->post('mobile_no');			
			$phone_code=$this->input->post('phone_code');
			
			$device_token=$this->input->post('device_token');
			$device_name=$this->input->post('device_name');
			
			$time_zone = $this->input->post('time_zone');
			$user_app_language = $this->input->post('user_app_language');
			
			$this->lang->load('authtext', $user_app_language);
			
			$email_otp=mt_rand(1000,9999);
			
			//code to check the valid mobile number
			$this->load->helper('sms_helper');
			$country_iso = getSingleValue('jp_countries','iso2','id',$country);
			$mobile_number =$phone_code.$mobile; 
			$is_valid_mobile = verify_mobile_number($country_iso,$mobile_number);
			// code ends here
			
			if($is_valid_mobile)
			{
				$user_data = array
				(
					'first_name'=>$first_name,
					'middle_name'=>$middle_name,
					'last_name'=>$last_name,
					'email'=>$email,
					'password'=>password_hash($password, PASSWORD_BCRYPT),
					'mobile_no'=>$mobile,
					'phone_code'=>$phone_code,
					'country'=>$country,
					'state'=>$state,
					'city'=>$city,
					'address'=>$autocomplete,
					'pincode'=>$postal_code,
					'latitude'=>$latitude,
					'longitude'=>$longitude,
					'device_token'=>$device_token,
					'device_name'=>$device_name,
					'email_otp'=>$email_otp,
					'created_at' => date('Y-m-d H:i:s'),
					'time_zone'=>$time_zone,
					'user_app_language'=>$user_app_language
				);
				
				$data = $this->security->xss_clean($user_data);
				$result = $this->auth_model->save_user_info($data);
				if($result){
					
					$user_role = 3;
					$secret_token=jwt_helper::create($result,$user_role);
				
					$mail_data['result']=array(
						"name" => $first_name.' '.$last_name,
						"password" => $password,
						"email_otp" => $email_otp
					);
					
					$user_data['user_token']=$secret_token;				
					
					$to=$email;
					$subject="Welcome User";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/user_welcome_register',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					// code to assign the job_seeker package
					$this->job_seeker_free_package($result);
					// code ends here
					
					$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$user_data,
					'message' =>$this->lang->line('userregister_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('register_fail_message')
						], REST_Controller::HTTP_OK);
				}
				
			}else{
				
					$this->response([
						'status' => false,
						'code' => 400,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('mobile_not_valid')	
					],REST_Controller::HTTP_OK);
				
			}		
		}
	}
	
	// function for user social login
	public function user_social_login_post()
	{
		$this->form_validation->set_rules('social_type', 'Social Type', 'trim|required');
		$this->form_validation->set_rules('oauth_uid', 'Auth userid', 'trim|required|xss_clean');
		$this->form_validation->set_rules('device_token', 'Device Token', 'trim|required');
		$this->form_validation->set_rules('device_name', 'Device Name', 'trim|required');
		
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		
		//$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[jp_users.email]|required');		
		
		$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
		
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		//$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
		
		$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
		$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
		
		$this->form_validation->set_rules('user_app_language', 'APP Language', 'trim|required');
				
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$social_type=$this->input->post('social_type');
			$oauth_uid=$this->input->post('oauth_uid');
			
			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			
			$email=$this->input->post('email');
							
			$latitude = $this->input->post('latitude');
			$longitude = $this->input->post('longitude');
			
			$mobile=$this->input->post('mobile_no');			
			$phone_code=$this->input->post('phone_code');
			
			$device_token=$this->input->post('device_token');
			$device_name=$this->input->post('device_name');
			
			$time_zone = $this->input->post('time_zone');
			$user_app_language = $this->input->post('user_app_language');
			$this->lang->load('authtext', $user_app_language);
			
			if($social_type=='gmail_social')
			{
				$data = array
				(
					'type'=>'gmail',
					'gmail_id' =>$oauth_uid
				);
			}
			
			if($social_type=='facebook_social')
			{
				$data = array
				(
					'type'=>'facebook',
					'facebook_id' =>$oauth_uid
				);
			}
			
			if($social_type=='linkd_social')
			{
				$data = array
				(
					'type'=>'linkdin',
					'linkd_in' =>$oauth_uid
				);
			}			
			
		
			$data = $this->security->xss_clean($data);
			$user_result = $this->auth_model->user_social_login($data);
			
			if(!empty($user_result))
			{
				$user_id = $user_result['user_id'];
				$user_email = $user_result['email'];
				$role_id = $user_result['user_role'];
				
				$phone_code = $user_result['phone_code'];
				$mobile_no = $user_result['mobile_no'];
				
				$user_name = $user_result['first_name'].' '.$user_result['last_name'];
				
				$is_resume = $this->auth_model->check_user_resume($user_id);
				$is_skills = $this->auth_model->check_user_skills($user_id);
				$is_education = $this->auth_model->check_user_education($user_id);
				$is_language = $this->auth_model->check_user_language($user_id);
				
				if(($is_resume) && ($is_skills) && ($is_education) && ($is_language))
				{
					$is_profile_updated='Y';
					
				}else{
					$is_profile_updated='N';
				}
				
				
				if(!empty($device_token))
				{
					$user_condition ="user_id = '$user_id'";					
					$token_data=array(
						'device_token'=>$device_token,
						'device_name'=>$device_name,
						'time_zone'=>$time_zone,
						'user_app_language'=>$user_app_language
					);
					
					$result = update_table('jp_users',$user_condition,$token_data);
				}
				
				$secret_token=jwt_helper::create($user_id,$role_id);
				$user_token=$secret_token;
				
				$user_profile = base_url().'public/dist/img/avatar5.png';
				if(!empty($user_result['profile_image']))
				{
					$user_profile = base_url().'public/images/user_docs/'.$user_result['profile_image'];
				}
				
				$country_name='';
				if(!empty($user_result['country']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$user_result['country']);
				}
				
				$state_name='';
				if(!empty($user_result['state']))
				{
					$state_name = getSingleValue('jp_states','name','id',$user_result['state']);
				}
				
				$city_name='';
				if(!empty($user_result['city']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$user_result['city']);
				}
				
				$advert_company_radius = $this->config->item('advert_company_radius');
				
				$user_data = array
				(
					'user_id' => $user_id,
					'user_name' => $user_result['user_name'],
					'first_name'=>$user_result['first_name'],
					'middle_name'=>$user_result['middle_name'],
					'last_name'=>$user_result['last_name'],
					'user_profile'=>$user_profile,
					'email'=>$user_email,
					'mobile_no' =>$user_result['mobile_no'],
					'phone_code'=>$user_result['phone_code'],
					'country'=>$country_name,
					'state'=>$state_name,
					'city'=>$city_name,
					'latitude'=>$user_result['latitude'],
					'longitude'=>$user_result['longitude'],
					'default_radius'=>$advert_company_radius,
					'is_email_verify'=>'Y',
					'is_mobile_verify'=>'Y',
					'user_token' =>$user_token,
					'is_profile_updated'=>$is_profile_updated,
					'time_zone'=>$user_result['time_zone'],
					'user_app_language'=>$user_result['user_app_language'],
					'is_user_login' => TRUE
				);

				if($user_result['is_mobile_verify'] == 'Y')
				{
					$user_data['mobile_verify']='Y';
					$user_data['mobile_otp']='';	
				}
				
				/*if($user_result['is_mobile_verify'] == 'N')
				{
					$user_data['mobile_verify']='N';
					
					$this->lang->load('smstext',$user_app_language);
					
					$full_mobile = $phone_code.$mobile_no;
					$sms_text = $this->lang->line('sms_otp');					
					$mobile_otp=mt_rand(1000,9999);
										
					$final_sms_text = sprintf($sms_text, $mobile_otp);
					
					$this->load->helper('sms_helper');
					$sms_status = send_sms($full_mobile, $final_sms_text);
					
					$handle_status = explode('*#',$sms_status);
					
					// Update User mobile OTP
					$usersms_otp_update=$this->users_model->usersms_otp_update($user_id, $mobile_otp);
					
					if($handle_status[0])
					{
						$user_data['mobile_otp']=$mobile_otp;
						
					}else{
							$this->response([
								'status' => true,
								'code' => 400,
								'data'=>$this->empty_object,
								'message' =>$handle_status[1]
							], REST_Controller::HTTP_OK);
					}						
				}*/
				
				if($user_result['is_email_verify'] == 'Y')
				{
					$user_data['email_verify']='Y';
					$user_data['email_otp']='';
				}
				
				$user_resumes=array();
				$user_data['resume_data']=$this->empty_object;
				$resumes_data = $this->users_model->resumes_data($user_id);
				if(!empty($resumes_data))
				{
					foreach($resumes_data as $resumes)
					{
						$user_resumes=array
						(
							'cv_id'=>$resumes['user_cv_id'],
							'cv_title'=>$resumes['user_cv_title'],
							'cv_file'=>$resumes['orignal_cv_name'],
							'cv_file_link'=>base_url().'public/images/user_docs/'.$resumes['user_cv_name']
						);
					}
					
					$user_data['resume_data']=$user_resumes;
				}
				

				/*if($user_result['is_email_verify'] == 'N')
				{
					$user_data['email_verify']='N';
					
					$email_otp=mt_rand(1000,9999);
					$mail_data['result']=array(
						"name" => $user_name,
						"email_tan" => $email_otp,
					);
					
					//Send Email With OTP To User
					$to=$user_email;
					$subject="Email OTP";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/email_tan',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					// Update User Email Tan
					$user_otp_update=$this->users_model->user_otp_update($user_id, $email_otp);
					if($emailsent && $user_otp_update)
					{
						$user_data['email_otp']=$email_otp;
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('forgot_email_not_sent')
							], REST_Controller::HTTP_OK);
					}
					
				}*/
				
				// code to get the users package info
				$user_pack = $this->user_check_license_expiration($user_id);
				$user_data['user_pack'] = $user_pack;
				// code ends here
				
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$user_data,
					'message'=>$this->lang->line('login_success_message')
				],REST_Controller::HTTP_OK);
			}else{
					// register through social
					$social_type=$this->input->post('social_type');
					$oauth_uid=$this->input->post('oauth_uid');
					
					$first_name=$this->input->post('first_name');
					$last_name=$this->input->post('last_name');
					$middle_name=$this->input->post('middle_name');
					
					$email=$this->input->post('email');
									
					//$password=$this->input->post('password');
								
					$country=$this->input->post('country');
					$state=$this->input->post('state');			
					$city=$this->input->post('city');
					
					$autocomplete=$this->input->post('address');
					$postal_code=$this->input->post('postal_code');
					
					$latitude = $this->input->post('latitude');
					$longitude = $this->input->post('longitude');
					
					$mobile=$this->input->post('mobile_no');			
					$phone_code=$this->input->post('phone_code');
					
					$device_token=$this->input->post('device_token');
					$device_name=$this->input->post('device_name');
					
					$time_zone = $this->input->post('time_zone');
					$user_app_language = $this->input->post('user_app_language');
					
					$this->lang->load('authtext', $user_app_language);
					
					$user_data = array
					(
						'first_name'=>$first_name,
						'middle_name'=>$middle_name,
						'last_name'=>$last_name,
						'email'=>$email,
						'mobile_no'=>$mobile,
						'phone_code'=>$phone_code,
						'country'=>$country,
						'state'=>$state,
						'city'=>$city,
						'address'=>$autocomplete,
						'pincode'=>$postal_code,
						'latitude'=>$latitude,
						'longitude'=>$longitude,
						'device_token'=>$device_token,
						'device_name'=>$device_name,
						'is_email_verify'=>'Y',
						'is_mobile_verify'=>'Y',
						'created_at' => date('Y-m-d H:i:s'),
						'time_zone'=>$time_zone,
						'user_app_language'=>$user_app_language
					);
					
					if($social_type=='gmail_social')
					{
						$user_data['gmail_id'] = $oauth_uid;						
					}
					
					if($social_type=='facebook_social')
					{
						$user_data['facebook_id'] = $oauth_uid;
					}
					
					if($social_type=='linkd_social')
					{
						$user_data['linkd_in'] = $oauth_uid;						
					}

						
					$cond="email = '$email'";
					$isexist=exists_indb('jp_users',$cond,'*');
					
					if(!empty($isexist))
					{
						$user_id = $isexist['user_id'];
						$social_condition="user_id = '$user_id'";
						update_table('jp_users',$social_condition,$user_data);
						
						$user_role = 3;
						$secret_token=jwt_helper::create($user_id,$user_role);
						
						$user_data['user_id']=$user_id;
						$user_data['user_token']=$secret_token;
						
						$is_resume = $this->auth_model->check_user_resume($user_id);
						$is_skills = $this->auth_model->check_user_skills($user_id);
						$is_education = $this->auth_model->check_user_education($user_id);
						$is_language = $this->auth_model->check_user_language($user_id);
						
						if(($is_resume) && ($is_skills) && ($is_education) && ($is_language))
						{
							$is_profile_updated='Y';
							
						}else{
							$is_profile_updated='N';
						}
						
						$user_data['is_profile_updated']=$is_profile_updated;
						
						$advert_company_radius = $this->config->item('advert_company_radius');
						$user_data['default_radius']=$advert_company_radius;
						
						$user_resumes=array();
						$user_data['resume_data']=$this->empty_object;
						$resumes_data = $this->users_model->resumes_data($user_id);
						if(!empty($resumes_data))
						{
							foreach($resumes_data as $resumes)
							{
								$user_resumes=array
								(
									'cv_id'=>$resumes['user_cv_id'],
									'cv_title'=>$resumes['user_cv_title'],
									'cv_file'=>$resumes['orignal_cv_name'],
									'cv_file_link'=>base_url().'public/images/user_docs/'.$resumes['user_cv_name']
								);
							}
							
							$user_data['resume_data']=$user_resumes;
						}
						
						$user_data['is_user_login'] = TRUE;
						
						// code to get the users package info
						$user_pack = $this->user_check_license_expiration($user_id);
						$user_data['user_pack'] = $user_pack;
						// code ends here
						
						$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$user_data,
						'message' =>$this->lang->line('login_success_message')
						],REST_Controller::HTTP_OK);
						
						
					}else{
							$data = $this->security->xss_clean($user_data);
							$result = $this->auth_model->save_user_info($data);
							if($result){
								
								$user_role = 3;
								$secret_token=jwt_helper::create($result,$user_role);
							
								$user_data['user_token']=$secret_token;				
								
								// code to assign the job_seeker package
								$this->job_seeker_free_package($result);
								// code ends here
								
								$is_resume = $this->auth_model->check_user_resume($result);
								$is_skills = $this->auth_model->check_user_skills($result);
								$is_education = $this->auth_model->check_user_education($result);
								$is_language = $this->auth_model->check_user_language($result);
								
								if(($is_resume) && ($is_skills) && ($is_education) && ($is_language))
								{
									$is_profile_updated='Y';
									
								}else{
									$is_profile_updated='N';
								}
								
								$user_data['is_profile_updated']=$is_profile_updated;
								
								$advert_company_radius = $this->config->item('advert_company_radius');
								$user_data['default_radius']=$advert_company_radius;
								
								$user_resumes=array();
								$user_data['resume_data']=$this->empty_object;
								$resumes_data = $this->users_model->resumes_data($result);
								if(!empty($resumes_data))
								{
									foreach($resumes_data as $resumes)
									{
										$user_resumes=array
										(
											'cv_id'=>$resumes['user_cv_id'],
											'cv_title'=>$resumes['user_cv_title'],
											'cv_file'=>$resumes['orignal_cv_name'],
											'cv_file_link'=>base_url().'public/images/user_docs/'.$resumes['user_cv_name']
										);
									}
									
									$user_data['resume_data']=$user_resumes;
								}
								
								// code to get the users package info
								$user_pack = $this->user_check_license_expiration($result);
								$user_data['user_pack'] = $user_pack;
								// code ends here
								
								$user_data['is_user_login'] = TRUE;
								
								$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$user_data,
								'message' =>$this->lang->line('login_success_message')
								],REST_Controller::HTTP_OK);
								
							}else{
									$this->response([
									'status' => true,
									'code' => 200,
									'data'=>$this->empty_object,
									'message' =>$this->lang->line('register_fail_message')
									], REST_Controller::HTTP_OK);
							}
						
					}
			}		
		}
	}
	// function ends here for social login
	
	public function job_seeker_free_package($result)
	{
		$package_data = getArrayValue('jp_packages','*','package_id',1);
		
		/*$timestamp = "2016-04-20 00:37:15";
		$start_date = date($timestamp);

		$expires = strtotime('+7 days', strtotime($timestamp));
		//$expires = date($expires);

		$date_diff=($expires-strtotime($timestamp)) / 86400;

		echo "Start: ".$timestamp."<br>";
		echo "Expire: ".date('Y-m-d H:i:s', $expires)."<br>";

		echo round($date_diff, 0)." days left";*/
		
		$user_free_package =array(
		'user_id'=>$result,
		'package_id'=>$package_data[0]['package_id'],
		'package_listings'=>$package_data[0]['package_listings'],
		'free_swipes'=>$package_data[0]['free_swipes'],
		'is_free'=>$package_data[0]['is_free'],
		'created_at'=>date('Y-m-d H:i:s')
		);
		
		$result = $this->auth_model->save_user_packageinfo($user_free_package);
	}
	
	

	public function user_login_post()
	{		
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' => preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))],REST_Controller::HTTP_OK);
		}else{
			
			$email=$this->input->post('email');
			$password= $this->input->post('password');
			
			$device_token=$this->input->post('device_token');
			$device_name=$this->input->post('device_name');
			$time_zone = $this->input->post('time_zone');
			$user_app_language = $this->input->post('user_app_language');
			
			$this->lang->load('authtext', $user_app_language);
			//$user_app_langauge = 'english';
			
			$data = array
			(
				'email' =>$email,
				'password' => $password
			);
		
			$data = $this->security->xss_clean($data);
			$user_result = $this->auth_model->user_login($data);
			
			if(!empty($user_result))
			{
				$user_id = $user_result['user_id'];
				$user_email = $user_result['email'];
				$role_id = $user_result['user_role'];
				
				$phone_code = $user_result['phone_code'];
				$mobile_no = $user_result['mobile_no'];
				
				$user_name = $user_result['first_name'].' '.$user_result['last_name'];
				
				$is_resume = $this->auth_model->check_user_resume($user_id);
				$is_skills = $this->auth_model->check_user_skills($user_id);
				$is_education = $this->auth_model->check_user_education($user_id);
				$is_language = $this->auth_model->check_user_language($user_id);
				
				if(($is_resume) && ($is_skills) && ($is_education) && ($is_language))
				{
					$is_profile_updated='Y';
					
				}else{
					$is_profile_updated='N';
				}
				
				
				if(!empty($device_token))
				{
					$user_condition ="user_id = '$user_id'";					
					$token_data=array(
						'device_token'=>$device_token,
						'device_name'=>$device_name,
						'time_zone'=>$time_zone,
						'user_app_language'=>$user_app_language
					);
					
					$result = update_table('jp_users',$user_condition,$token_data);
				}
				
				$secret_token=jwt_helper::create($user_id,$role_id);
				$user_token=$secret_token;
				
				$user_profile = base_url().'public/dist/img/avatar5.png';
				if(!empty($user_result['profile_image']))
				{
					$user_profile = base_url().'public/images/user_docs/'.$user_result['profile_image'];
				}
				
				$country_name='';
				if(!empty($user_result['country']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$user_result['country']);
				}
				
				$state_name='';
				if(!empty($user_result['state']))
				{
					$state_name = getSingleValue('jp_states','name','id',$user_result['state']);
				}
				
				$city_name='';
				if(!empty($user_result['city']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$user_result['city']);
				}
				
				$advert_company_radius = $this->config->item('advert_company_radius');
				
				$user_data = array
				(
					'user_id' => $user_id,
					'user_name' => $user_result['user_name'],
					'first_name'=>$user_result['first_name'],
					'middle_name'=>$user_result['middle_name'],
					'last_name'=>$user_result['last_name'],
					'user_profile'=>$user_profile,
					'email'=>$user_email,
					'mobile_no' =>$user_result['mobile_no'],
					'phone_code'=>$user_result['phone_code'],
					'country'=>$country_name,
					'state'=>$state_name,
					'city'=>$city_name,
					'latitude'=>$user_result['latitude'],
					'longitude'=>$user_result['longitude'],
					'default_radius'=>$advert_company_radius,
					'user_token' =>$user_token,
					'is_profile_updated'=>$is_profile_updated,
					'is_user_login' => TRUE
				);

				if($user_result['is_mobile_verify'] == 'Y')
				{
					$user_data['mobile_verify']='Y';
					$user_data['mobile_otp']='';	
				}
				
				if($user_result['is_mobile_verify'] == 'N')
				{
					$user_data['mobile_verify']='N';
					
					$this->lang->load('smstext',$user_app_language);
					
					$full_mobile = $phone_code.$mobile_no;
					$sms_text = $this->lang->line('sms_otp');					
					$mobile_otp=mt_rand(1000,9999);
										
					$final_sms_text = sprintf($sms_text, $mobile_otp);
					
					$this->load->helper('sms_helper');
					$sms_status = send_sms($full_mobile, $final_sms_text);
					
					$handle_status = explode('*#',$sms_status);
					
					// Update User mobile OTP
					$usersms_otp_update=$this->users_model->usersms_otp_update($user_id, $mobile_otp);
					
					if($handle_status[0])
					{
						$user_data['mobile_otp']=$mobile_otp;
						
					}else{
							$this->response([
								'status' => true,
								'code' => 400,
								'data'=>$this->empty_object,
								'message' =>$handle_status[1]
							], REST_Controller::HTTP_OK);
					}						
				}
				
				if($user_result['is_email_verify'] == 'Y')
				{
					$user_data['email_verify']='Y';
					$user_data['email_otp']='';
				}
				
				$user_resumes=array();
				$user_data['resume_data']=$this->empty_object;
				$resumes_data = $this->users_model->resumes_data($user_id);
				if(!empty($resumes_data))
				{
					foreach($resumes_data as $resumes)
					{
						$user_resumes=array
						(
							'cv_id'=>$resumes['user_cv_id'],
							'cv_title'=>$resumes['user_cv_title'],
							'cv_file'=>$resumes['orignal_cv_name'],
							'cv_file_link'=>base_url().'public/images/user_docs/'.$resumes['user_cv_name']
						);
					}
					
					$user_data['resume_data']=$user_resumes;
				}
				

				if($user_result['is_email_verify'] == 'N')
				{
					$user_data['email_verify']='N';
					
					$email_otp=mt_rand(1000,9999);
					$mail_data['result']=array(
						"name" => $user_name,
						"email_tan" => $email_otp,
					);
					
					//Send Email With OTP To User
					$to=$user_email;
					$subject="Email OTP";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/email_tan',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					// Update User Email Tan
					$user_otp_update=$this->users_model->user_otp_update($user_id, $email_otp);
					if($emailsent && $user_otp_update)
					{
						$user_data['email_otp']=$email_otp;
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('forgot_email_not_sent')
							], REST_Controller::HTTP_OK);
					}
					
				}
				
				// code to get the users package info
				$user_pack = $this->user_check_license_expiration($user_id);
				$user_data['user_pack'] = $user_pack;
				// code ends here
				
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$user_data,
					'message'=>$this->lang->line('login_success_message')
				],REST_Controller::HTTP_OK);
			}else{
				
				$this->response([
					'status' => false,
					'code' => 400,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('login_pwd_email_wrong')
				],REST_Controller::HTTP_OK);
			}			
		}			
	}
	
	public function company_register_post()
	{
		$this->form_validation->set_rules('company_first_name','First Name', 'trim|required');
		$this->form_validation->set_rules('company_last_name', 'Last Name', 'trim|required');
		
		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		
		$this->form_validation->set_rules('company_email', 'Company Email', 'trim|valid_email|is_unique[jp_companies.company_email]|required');
		$this->form_validation->set_rules('company_password', 'Password', 'trim|required');
		
		$this->form_validation->set_rules('company_country', 'Country', 'trim|required');
		$this->form_validation->set_rules('company_state', 'State', 'trim|required');
		$this->form_validation->set_rules('company_city', 'City', 'trim|required');
		
		$this->form_validation->set_rules('company_phone', 'Mobile Number', 'trim|required');
		
		$this->form_validation->set_rules('company_address', 'Address', 'trim|required');
		//$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
		
		$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
		$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
		
		$this->form_validation->set_rules('device_token', 'Device Token', 'trim|required');
		$this->form_validation->set_rules('device_name', 'Device Name', 'trim|required');
		
		$this->form_validation->set_rules('user_app_language', 'APP Language', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' => preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))],REST_Controller::HTTP_OK);
		}else{
				$company_first_name=$this->input->post('company_first_name');
				$company_last_name=$this->input->post('company_last_name');
				
				$company_email=$this->input->post('company_email');
				$company_name=$this->input->post('company_name');
				
				$company_password=$this->input->post('company_password');
				
				$company_phone=$this->input->post('company_phone');				
				$company_phone_code = $this->input->post('company_phone_code');
								
				$company_country=$this->input->post('company_country');
				$company_state=$this->input->post('company_state');				
				$company_city=$this->input->post('company_city');
				
				$autocomplete=$this->input->post('autocomplete');
				$company_latitude = $this->input->post('latitude');
				$company_longitude = $this->input->post('longitude');
				
				$device_token=$this->input->post('device_token');
				$device_name=$this->input->post('device_name');
				
				$time_zone = $this->input->post('time_zone');
				$user_app_language = $this->input->post('user_app_language');
				
				$this->lang->load('authtext', $user_app_language);
				
				$email_otp=mt_rand(1000,9999);
				
				//code to check the valid mobile number
					$this->load->helper('sms_helper');
					$country_iso = getSingleValue('jp_countries','iso2','id',$company_country);
					$mobile_number =$company_phone_code.$company_phone; 
					$is_valid_mobile = verify_mobile_number($country_iso,$mobile_number);
				// code ends here
				
				if($is_valid_mobile)
				{
					$company_data = array
				(
					'company_first_name'=>$company_first_name,
					'company_last_name'=>$company_last_name,
					'company_email'=>$company_email,
					'company_name'=>$company_name,
					'company_password'=>password_hash($company_password, PASSWORD_BCRYPT),
					'company_country'=>$company_country,
					'company_state'=>$company_state,
					'company_city'=>$company_city,
					'company_address'=>$autocomplete,
					'company_latitude'=>$company_latitude,
					'company_longitude'=>$company_longitude,
					'company_phone_code'=>$company_phone_code,
					'company_phone'=>$company_phone,
					'device_token'=>$device_token,
					'device_name'=>$device_name,
					'email_otp'=>$email_otp,
					'company_created_at' => date('Y-m-d H:i:s'),
					'time_zone'=>$time_zone,
					'company_app_language'=>$user_app_language
				);
				
				$data = $this->security->xss_clean($company_data);
				$result = $this->auth_model->save_company_info($data);
				
				if($result)
				{
					$user_role = 2;
					$secret_token=jwt_helper::create($result,$user_role);
					
					$company_data['user_token']=$secret_token;
				
					/*$user_response=array(
						'user_token'=>$secret_token,
						'email_otp'=>$email_otp
					);*/
					
					$mail_data['result']=array(
						"name" => $company_name,
						"password" => $company_password,
						"email_otp" => $email_otp
					);
					
					$to=$company_email;
					$subject="Welcome Company";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/user_welcome_register',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					// code to assign the job_seeker package
					$this->job_recruiter_free_package($result);
					// code ends here
					
					$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$company_data,
					'message' =>$this->lang->line('companyregister_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('register_fail_message')
						], REST_Controller::HTTP_OK);
				}
					
				}else{
					
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('mobile_not_valid')
						],REST_Controller::HTTP_OK);
					
				}
		}
	}
	
	// function for company social login
	public function company_social_login_post()
	{
		$this->form_validation->set_rules('social_type', 'Social Type', 'trim|required');
		$this->form_validation->set_rules('oauth_uid', 'Auth userid', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('company_first_name','First Name', 'trim|required');
		$this->form_validation->set_rules('company_last_name', 'Last Name', 'trim|required');
		
		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		
		//$this->form_validation->set_rules('company_email', 'Company Email', 'trim|valid_email|is_unique[jp_companies.company_email]|required');
		//$this->form_validation->set_rules('company_password', 'Password', 'trim|required');
		
		$this->form_validation->set_rules('company_country', 'Country', 'trim|required');
		$this->form_validation->set_rules('company_state', 'State', 'trim|required');
		$this->form_validation->set_rules('company_city', 'City', 'trim|required');
		
		$this->form_validation->set_rules('company_phone', 'Mobile Number', 'trim|required');
		
		$this->form_validation->set_rules('company_address', 'Address', 'trim|required');
		//$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
		
		$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
		$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
		
		$this->form_validation->set_rules('device_token', 'Device Token', 'trim|required');
		$this->form_validation->set_rules('device_name', 'Device Name', 'trim|required');
		
		$this->form_validation->set_rules('user_app_language', 'APP Language', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$social_type=$this->input->post('social_type');
			$oauth_uid=$this->input->post('oauth_uid');
			
			$company_first_name=$this->input->post('company_first_name');
			$company_last_name=$this->input->post('company_last_name');
			
			$company_email=$this->input->post('company_email');
			$company_name=$this->input->post('company_name');
			
			$company_password=$this->input->post('company_password');
			
			$company_phone=$this->input->post('company_phone');				
			$company_phone_code = $this->input->post('company_phone_code');
							
			$company_country=$this->input->post('company_country');
			$company_state=$this->input->post('company_state');				
			$company_city=$this->input->post('company_city');
			
			$autocomplete=$this->input->post('autocomplete');
			$company_latitude = $this->input->post('latitude');
			$company_longitude = $this->input->post('longitude');
			
			$device_token=$this->input->post('device_token');
			$device_name=$this->input->post('device_name');
			
			$time_zone = $this->input->post('time_zone');
			$user_app_language = $this->input->post('user_app_language');
			
			$this->lang->load('authtext', $user_app_language);
			
			if($social_type=='gmail_social')
			{
				$data = array
				(
					'type'=>'gmail',
					'gmail_id' =>$oauth_uid
				);
			}
			
			if($social_type=='facebook_social')
			{
				$data = array
				(
					'type'=>'facebook',
					'facebook_id' =>$oauth_uid
				);
			}
			
			if($social_type=='linkd_social')
			{
				$data = array
				(
					'type'=>'linkdin',
					'linkd_in' =>$oauth_uid
				);
			}			
			
		
			$data = $this->security->xss_clean($data);
			$user_result = $this->auth_model->company_social_login($data);
			
			if(!empty($user_result))
			{
				$user_id = $user_result['company_id'];
				$user_email = $user_result['company_email'];
				$role_id = $user_result['user_role'];
				
				$phone_code = $user_result['company_phone_code'];
				$mobile_no = $user_result['company_phone'];
				
				$user_name = $user_result['company_name'];
				$time_zone = $this->input->post('time_zone');
				$user_app_language = $this->input->post('user_app_language');
				
				$this->lang->load('authtext', $user_app_language);
				
				if(!empty($device_token))
				{
					$user_condition ="company_id = '$user_id'";					
					$token_data=array(
						'device_token'=>$device_token,
						'device_name'=>$device_name,
						'time_zone'=>$time_zone,
						'company_app_language'=>$user_app_language
					);
					
					$result = update_table('jp_companies',$user_condition,$token_data);
				}
				
				//print_r($user_result);
				
				$secret_token=jwt_helper::create($user_id,$role_id);				
				$user_token=$secret_token;
				
				$user_profile = base_url().'public/images/company_app_no_logo.png';
				if(!empty($user_result['company_logo']))
				{
					$user_profile = base_url().'public/images/company_docs/'.$user_result['company_logo'];
				}
				
				$country_name='';
				if(!empty($user_result['company_country']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$user_result['company_country']);
				}
				
				$state_name='';
				if(!empty($user_result['company_state']))
				{
					$state_name = getSingleValue('jp_states','name','id',$user_result['company_state']);
				}
				
				$city_name='';
				if(!empty($user_result['company_city']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$user_result['company_city']);
				}
				
				$advert_company_radius = $this->config->item('advert_company_radius');
				$user_data = array
				(
					'company_id' => $user_id,
					'company_name' => $user_result['company_name'],
					'first_name'=>$user_result['company_first_name'],
					'last_name'=>$user_result['company_last_name'],
					'company_logo'=>$user_profile,
					'email'=>$user_email,
					'mobile_no' =>$user_result['company_phone'],
					'phone_code'=>$user_result['company_phone_code'],
					'country'=>$country_name,
					'state'=>$state_name,
					'city'=>$city_name,
					'latitude'=>$user_result['company_latitude'],
					'longitude'=>$user_result['company_longitude'],
					'default_radius'=>$advert_company_radius,
					'email_verify'=>$user_result['is_email_verify'],
					'mobile_verify'=>$user_result['is_mobile_verify'],
					'user_token' =>$user_token,
					'is_user_login' => TRUE
				);	

				if($user_result['is_mobile_verify'] == 'Y')
				{
					$user_data['mobile_verify']='Y';
					$user_data['mobile_otp']='';	
				}
				
				if($user_result['is_email_verify'] == 'Y')
				{
					$user_data['email_verify']='Y';
					$user_data['email_otp']='';
				}

				// code to get the users package info
				$user_pack = $this->company_check_license_expiration($user_id);
				$user_data['company_pack'] = $user_pack;
				// code ends here	
				
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$user_data,
					'message'=>$this->lang->line('login_success_message')
				],REST_Controller::HTTP_OK);
			}else{
				
				$social_type=$this->input->post('social_type');
				$oauth_uid=$this->input->post('oauth_uid');
				
				$company_first_name=$this->input->post('company_first_name');
				$company_last_name=$this->input->post('company_last_name');
				
				$company_email=$this->input->post('company_email');
				$company_name=$this->input->post('company_name');
				
								
				$company_phone=$this->input->post('company_phone');				
				$company_phone_code = $this->input->post('company_phone_code');
								
				$company_country=$this->input->post('company_country');
				$company_state=$this->input->post('company_state');				
				$company_city=$this->input->post('company_city');
				
				$autocomplete=$this->input->post('autocomplete');
				$company_latitude = $this->input->post('latitude');
				$company_longitude = $this->input->post('longitude');
				
				$device_token=$this->input->post('device_token');
				$device_name=$this->input->post('device_name');
				
				$time_zone = $this->input->post('time_zone');
				$user_app_language = $this->input->post('user_app_language');
				
				$this->lang->load('authtext', $user_app_language);	

				$user_profile = base_url().'public/images/company_app_no_logo.png';	
				$advert_company_radius = $this->config->item('advert_company_radius');
				
				$user_data = array
				(
					'company_name' => $company_name,
					'first_name'=>$company_first_name,
					'last_name'=>$company_last_name,
					'company_logo'=>$user_profile,
					'email'=>$company_email,
					'mobile_no' =>$company_phone,
					'phone_code'=>$company_phone_code,
					'country'=>$company_country,
					'state'=>$company_state,
					'city'=>$company_city,
					'latitude'=>$company_latitude,
					'longitude'=>$company_longitude,
					'default_radius'=>$advert_company_radius,
					'is_email_verify'=>'Y',
					'is_mobile_verify'=>'Y',
					'is_user_login' => TRUE
				);	
				
				$company_data = array
				(
					'company_first_name'=>$company_first_name,
					'company_last_name'=>$company_last_name,
					'company_email'=>$company_email,
					'company_name'=>$company_name,
					'company_country'=>$company_country,
					'company_state'=>$company_state,
					'company_city'=>$company_city,
					'company_address'=>$autocomplete,
					'company_latitude'=>$company_latitude,
					'company_longitude'=>$company_longitude,
					'company_phone_code'=>$company_phone_code,
					'company_phone'=>$company_phone,
					'device_token'=>$device_token,
					'device_name'=>$device_name,
					'is_email_verify'=>'Y',
					'is_mobile_verify'=>'Y',
					'company_created_at' => date('Y-m-d H:i:s'),
					'time_zone'=>$time_zone,
					'company_app_language'=>$user_app_language
				);
				
				if($social_type=='gmail_social')
				{
					$company_data['gmail_id'] = $oauth_uid;
				}
				
				if($social_type=='facebook_social')
				{
					$company_data['facebook_id'] = $oauth_uid;					
				}
				
				if($social_type=='linkd_social')
				{
					$company_data['linkd_in'] = $oauth_uid;					
				}
				
				$cond="company_email = '$company_email'";
				$isexist=exists_indb('jp_companies',$cond,'*');
				
				if(!empty($isexist))
				{
					$user_id = $isexist['company_id'];
					$social_condition="company_id = '$user_id'";
					update_table('jp_companies',$social_condition,$company_data);
					
					$user_role = 2;
					$secret_token=jwt_helper::create($user_id,$user_role);
					
					$user_data['company_id'] = $user_id;
					$user_data['user_token']=$secret_token;
					
					$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$user_data,
					'message' =>$this->lang->line('login_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
					
						$data = $this->security->xss_clean($company_data);
						$result = $this->auth_model->save_company_info($data);
						
						if($result)
						{
							$user_role = 2;
							$secret_token=jwt_helper::create($result,$user_role);
							
							$user_data['company_id'] = $result;
							$user_data['user_token']=$secret_token;
						
							// code to assign the job_seeker package
							$this->job_recruiter_free_package($result);
							// code ends here
							
							$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$user_data,
							'message' =>$this->lang->line('login_success_message')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('register_fail_message')
								], REST_Controller::HTTP_OK);
						}					
				}
			}					
		}
		
	}
	// function ends here for company social login
	
	public function job_recruiter_free_package($result)
	{
		$package_data = getArrayValue('jp_packages','*','package_id',2);
		
		$company_free_package =array(
		'company_id'=>$result,
		'package_id'=>$package_data[0]['package_id'],
		'package_listings'=>$package_data[0]['package_listings'],
		'free_swipes'=>$package_data[0]['free_swipes'],
		'is_free'=>$package_data[0]['is_free'],
		'created_at'=>date('Y-m-d H:i:s')
		);
		
		$result = $this->auth_model->job_recruiter_free_package($company_free_package);
	}
	
	public function company_login_post()
	{		
		$empty_data = array();
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' => preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))],REST_Controller::HTTP_OK);
		}else{
			
			$email=$this->input->post('email');
			$password= $this->input->post('password');
			$device_token=$this->input->post('device_token');
			$device_name=$this->input->post('device_name');
			
			$data = array
			(
				'email' =>$email,
				'password' => $password
			);
		
			$data = $this->security->xss_clean($data);
			$user_result = $this->auth_model->company_login($data);
			
			if(!empty($user_result))
			{
				$user_id = $user_result['company_id'];
				$user_email = $user_result['company_email'];
				$role_id = $user_result['user_role'];
				
				$phone_code = $user_result['company_phone_code'];
				$mobile_no = $user_result['company_phone'];
				
				$user_name = $user_result['company_name'];
				$time_zone = $this->input->post('time_zone');
				$user_app_language = $this->input->post('user_app_language');
				
				$this->lang->load('authtext', $user_app_language);
				
				if(!empty($device_token))
				{
					$user_condition ="company_id = '$user_id'";					
					$token_data=array(
						'device_token'=>$device_token,
						'device_name'=>$device_name,
						'time_zone'=>$time_zone,
						'company_app_language'=>$user_app_language
					);
					
					$result = update_table('jp_companies',$user_condition,$token_data);
				}
				
				//print_r($user_result);
				
				$secret_token=jwt_helper::create($user_id,$role_id);				
				$user_token=$secret_token;
				
				$user_profile = base_url().'public/images/company_app_no_logo.png';
				if(!empty($user_result['company_logo']))
				{
					$user_profile = base_url().'public/images/company_docs/'.$user_result['company_logo'];
				}
				
				$country_name='';
				if(!empty($user_result['company_country']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$user_result['company_country']);
				}
				
				$state_name='';
				if(!empty($user_result['company_state']))
				{
					$state_name = getSingleValue('jp_states','name','id',$user_result['company_state']);
				}
				
				$city_name='';
				if(!empty($user_result['company_city']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$user_result['company_city']);
				}
				
				$advert_company_radius = $this->config->item('advert_company_radius');
				$user_data = array
				(
					'company_id' => $user_id,
					'company_name' => $user_result['company_name'],
					'first_name'=>$user_result['company_first_name'],
					'last_name'=>$user_result['company_last_name'],
					'company_logo'=>$user_profile,
					'email'=>$user_email,
					'mobile_no' =>$user_result['company_phone'],
					'phone_code'=>$user_result['company_phone_code'],
					'country'=>$country_name,
					'state'=>$state_name,
					'city'=>$city_name,
					'latitude'=>$user_result['company_latitude'],
					'longitude'=>$user_result['company_longitude'],
					'default_radius'=>$advert_company_radius,
					'email_verify'=>$user_result['is_email_verify'],
					'mobile_verify'=>$user_result['is_mobile_verify'],
					'user_token' =>$user_token,
					'is_user_login' => TRUE
				);	

				if($user_result['is_mobile_verify'] == 'Y')
				{
					$user_data['mobile_verify']='Y';
					$user_data['mobile_otp']='';	
				}
				
				if($user_result['is_mobile_verify'] == 'N')
				{
					$user_data['mobile_verify']='N';
					
					$this->lang->load('smstext',$user_app_language);
					
					$full_mobile = $phone_code.$mobile_no;
					$sms_text = $this->lang->line('sms_otp');					
					$mobile_otp=mt_rand(1000,9999);
										
					$final_sms_text = sprintf($sms_text, $mobile_otp);
					
					$this->load->helper('sms_helper');
					$sms_status = send_sms($full_mobile, $final_sms_text);
					
					$handle_status = explode('*#',$sms_status);
					
					// Update User mobile OTP
					$usersms_otp_update=$this->companies_model->companysms_otp_update($user_id, $mobile_otp);
					
					if($handle_status[0])
					{
						$user_data['mobile_otp']=$mobile_otp;
						
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$handle_status[1]
							], REST_Controller::HTTP_OK);
					}						
				}
				
				if($user_result['is_email_verify'] == 'Y')
				{
					$user_data['email_verify']='Y';
					$user_data['email_otp']='';
				}

				if($user_result['is_email_verify'] == 'N')
				{
					$user_data['email_verify']='N';
					
					$email_otp=mt_rand(1000,9999);
					$mail_data['result']=array(
						"name" => $user_name,
						"email_tan" => $email_otp,
					);
					
					//Send Email With OTP To User
					$to=$user_email;
					$subject="Email OTP";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/email_tan',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					// Update User Email Tan
					$user_otp_update=$this->companies_model->user_otp_update($user_id, $email_otp);
					if($emailsent && $user_otp_update)
					{
						$user_data['email_otp']=$email_otp;
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('forgot_email_not_sent')
							], REST_Controller::HTTP_OK);
					}
					
				}
					
				// code to get the users package info
				$user_pack = $this->company_check_license_expiration($user_id);
				$user_data['company_pack'] = $user_pack;
				// code ends here	
				
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$user_data,
					'message'=>$this->lang->line('login_success_message')
				],REST_Controller::HTTP_OK);
			}else{
				
				$this->response([
					'status' => false,
					'code' => 400,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('login_pwd_email_wrong')
				],REST_Controller::HTTP_OK);
			}			
		}			
	}
	
	// function to check the package for user
	public function user_check_license_expiration($user_id)
	{
		$package_data = array();
		$license_cond="user_id = '".$user_id."'";
		$islicense_exist=exists_indb('jp_users_packages',$license_cond,'*');
		
		if(!empty($islicense_exist))
		{
			$license_ends_at = $islicense_exist['package_end'];
			$license_ends_stamp = strtotime($license_ends_at);
						
			$license_ends_at = date('Y-m-d H:i:s', $license_ends_stamp);
			$today = date("Y-m-d H:i:s");
			
			$today_dt = new DateTime($today);
			$expire_dt = new DateTime($license_ends_at);
			
			if($today_dt < $expire_dt)
			{  
				$package_expire ='N';
			}else{
				$package_expire ='Y';				
			}
			
			$package_data = array(
				'user_package_id'=>$islicense_exist['user_package_id'],
				'package_id'=>$islicense_exist['package_id'],
				'job_view'=>$islicense_exist['package_listings'],
				'total_swipes_available'=>$islicense_exist['free_swipes'],
				'used_swipes'=>$islicense_exist['used_swipes'],
				'package_expire'=>$package_expire,
				'expire_date'=>$islicense_exist['package_end']
			);
		}
		
		return $package_data;
				
	}
	// function ends here
	
	// function to check the package for company
	public function company_check_license_expiration($company_id)
	{
		$package_data = array();
		$license_cond="company_id = '".$company_id."'";
		$islicense_exist=exists_indb('jp_company_job_packages',$license_cond,'*');
		
		if(!empty($islicense_exist))
		{
			$license_ends_at = $islicense_exist['package_end'];
			$license_ends_stamp = strtotime($license_ends_at);
						
			$license_ends_at = date('Y-m-d H:i:s', $license_ends_stamp);
			$today = date("Y-m-d H:i:s");
			
			$today_dt = new DateTime($today);
			$expire_dt = new DateTime($license_ends_at);
			
			if($today_dt < $expire_dt)
			{  
				$package_expire ='N';
			}else{
				$package_expire ='Y';				
			}
			
			$package_data = array(
				'company_package_id'=>$islicense_exist['company_package_id'],
				'package_id'=>$islicense_exist['package_id'],
				'total_available_job_listing'=>$islicense_exist['package_listings'],
				'used_job_listing'=>$islicense_exist['used_listings'],
				'total_swipes_available'=>$islicense_exist['free_swipes'],
				'used_swipes'=>$islicense_exist['used_swipes'],
				'package_expire'=>$package_expire,
				'expire_date'=>$islicense_exist['package_end']
			);
		}
		
		return $package_data;
				
	}
	// function ends here
}