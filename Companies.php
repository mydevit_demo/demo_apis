<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');

// Use the REST API Client to make requests to the Twilio REST API
//use Twilio\Rest\Client;

class Companies extends REST_Controller
{
	public $empty_object;
	function __construct(){
		parent::__construct();
		$this->load->helper('jwt_helper');
		$this->load->model('app_api/companies_model', 'companies_model');
		$this->load->model('app_api/users_model', 'users_model');
		$this->load->helper('email_helper');
		$this->load->helper('fcmpush_helper');
		//$this->load->helper('string');
		$this->lang->load('authtext');
		$this->empty_object = json_decode('{}');	
	}
	
	public function company_data_get()
	{
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$company_info = $this->companies_model->company_data($user_id);
		
		if(!empty($company_info))
		{
			$company_logo = base_url().'public/images/company_app_no_logo.png';
			if(!empty($company_info['company_logo']))
			{
				$company_logo = base_url().'public/images/company_docs/'.$company_info['company_logo'];	
			}
			
			$company_video = '';
			if(!empty($company_info['company_video']))
			{
				$company_video = base_url().'public/images/company_docs/'.$company_info['company_video'];	
			}
			
			$country_name='';
			if(!empty($company_info['company_country']))
			{
				$country_name = getSingleValue('jp_countries','name','id',$company_info['company_country']);
			}
			
			$state_name='';
			if(!empty($company_info['company_state']))
			{
				$state_name = getSingleValue('jp_states','name','id',$company_info['company_state']);
			}
			
			$city_name='';
			if(!empty($company_info['company_city']))
			{
				$city_name = getSingleValue('jp_cities','name','id',$company_info['company_city']);
			}
			
			$industry_name='';
			if(!empty($company_info['industry_id']))
			{
				$industry_name = getSingleValue('jp_industries','industry_name','industry_id',$company_info['industry_id']);
			}
			
			$company_size='';
			if(!empty($company_info['company_employees']))
			{
				$company_size = $company_info['company_employees'];
			}
			
			$company_founded='';
			if(!empty($company_info['company_established']))
			{
				$company_founded = $company_info['company_established'];
			}
			
			$company_url='';
			if(!empty($company_info['company_website']))
			{
				$company_url = $company_info['company_website'];
			}
			
			$advert_company_radius = $this->config->item('advert_company_radius');			
			
			$company_data = array
			(
				'company_id'=>$company_info['company_id'],
				'first_name'=>$company_info['company_first_name'],
				'last_name'=>$company_info['company_last_name'],
				'company_name'=>$company_info['company_name'],
				'company_email'=>$company_info['company_email'],
				'company_logo'=>$company_logo,
				'company_video'=>$company_video,
				'company_address'=>$company_info['company_address'],
				'country'=>$country_name,
				'state'=>$state_name,
				'city'=>$city_name,
				'company_pincode'=>$company_info['company_pincode'],
				'phone_code'=>$company_info['company_phone_code'],
				'mobile'=>$company_info['company_phone'],
				'industry_name'=>$industry_name,
				'company_size'=>$company_size,
				'company_founded'=>$company_founded,
				'company_url'=>$company_url,
				'latitude'=>$company_info['company_latitude'],
				'longitude'=>$company_info['company_longitude'],
				'default_radius'=>$advert_company_radius,
				'is_email_verify'=>$company_info['is_email_verify'],
				'is_mobile_verify'=>$company_info['is_mobile_verify'],
				'review_rating'=>array(array('rating_title'=>'Work Life Balance','rating'=>'4.3'),array('rating_title'=>'Pay & Benefits','rating'=>'4.0'),array('rating_title'=>'Job Security','rating'=>'3.5'),array('rating_title'=>'Culture & Management','rating'=>'4.2'),array('rating_title'=>'Interview','rating'=>'4.5'))
			);
		}
		
		$this->response([
			'status' => true,
			'code' => 200,
			'data'=>$company_data,
			'message' =>'Data fetched successfully'
		],REST_Controller::HTTP_OK);
	}
	
	// function to send the email OTP
	public function verify_company_email_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		//$this->form_validation->set_rules('email', 'Email', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
				'status' => false,
				'code' => 400,
				'data'=>$this->empty_object,
				'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
				//$user_email=$this->input->post('email');
			
				$user_data = jwt_helper::decode($user_token);
				
				$user_id = $user_data->userId;
				
				$checker_data=array
				(
					'user_id'=>$user_id
				);
				
				$user_checker = $this->companies_model->check_user($checker_data);
				
				if($user_checker)
				{
					$user_name = $user_checker['company_name'];
					$email_otp=mt_rand(1000,9999);
					$mail_data['result']=array(
						"name" => $user_name,
						"email_tan" => $email_otp,
					);
					
					//Send Email With OTP To User
					$to=$user_checker['company_email'];
					$subject="Email OTP";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/email_tan',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					$emailsent=true;
					
					// Update User Email Tan
					$user_otp_update=$this->companies_model->user_otp_update($user_id, $email_otp);
					if($emailsent && $user_otp_update)
					{
						$data=array(
							'email_otp'=>$email_otp
						);
				
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$data,
							'message' =>'Please check mail for OTP'
						],REST_Controller::HTTP_OK);
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>'Email not sent, Try again later'
							], REST_Controller::HTTP_OK);
					}
					
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>'User not exist for this email'
						], REST_Controller::HTTP_OK);
				}				
		}	
	}
	// function for email tan ends here
	
	// function to varify the email tan
	public function email_verify_otp_post()
	{
		$this->form_validation->set_rules('email_otp', 'Email OTP', 'trim|required');
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$email_otp=$this->input->post('email_otp');
				$user_token=$this->input->post('user_token');
			
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$otp_data=array(
					'user_id'=>$user_id,
					'email_otp'=>$email_otp
				);
				
				$email_otp_verify = $this->companies_model->email_otp_verify($otp_data);
				
				if($email_otp_verify)
				{
					//$this->lang->load('smstext');
				
					$cond="company_id = '".$user_id."'";
					$isexist=exists_indb('jp_companies',$cond,'company_phone_code, company_phone,is_mobile_verify,company_app_language');
					
					$mobile_otp='';
					if($isexist['is_mobile_verify']=='N')
					{
						$user_app_langauge = $isexist['company_app_language'];
						$this->lang->load('authtext', $user_app_langauge);
						
						$this->lang->load('smstext',$user_app_langauge);
						
						$mobile_otp=mt_rand(1000,9999);
						
						$data_email=array('mobile_otp' => $mobile_otp);
						
						$new_condition="company_id='".$user_id."'";
						$update_mobile=update_table('jp_companies',$new_condition,$data_email);
						
						$full_mobile = $isexist['company_phone_code'].$isexist['company_phone'];
						$sms_text = $this->lang->line('sms_otp');					
															
						$final_sms_text = sprintf($sms_text, $mobile_otp);
						
						$this->load->helper('sms_helper');
						$sms_status = send_sms($full_mobile, $final_sms_text);	
					}					
					
					$data=array(
						'mobile_otp'=>$mobile_otp
					);
					
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('email_success')
					],REST_Controller::HTTP_OK);
				}else{
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('email_verify_wrong_tan')
						], REST_Controller::HTTP_OK);
				}
		}	
	}
	// function ends here
	
	// function to edit the company email	
	public function company_edit_email_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[jp_users.email]|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
		],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
			
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$email  = $this->input->post('email');
				
				$cond="company_id = '".$user_id."'";
				$isexist=exists_indb('jp_companies',$cond,'company_name,company_app_language');
				
				$user_app_langauge = $isexist['company_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$email_otp=mt_rand(1000,9999);
				$user_name = $isexist['company_name'];
				
				$mail_data['result']=array(
					"name" => $user_name,
					"email_tan" => $email_otp,
				);
						
				$data_email=array('company_email' => $email, 'email_otp' => $email_otp);
				$new_condition="company_id='".$user_id."'";
				$update_mobile=update_table('jp_companies',$new_condition,$data_email);
				
				//Send Email With OTP To User
				$to=$email;
				$subject="Email OTP";
				$attachment='';
				$cc='';
				$body=$this->load->view('email_template/email_tan',$mail_data,true);
				$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
								
				$data=array(
				'email_otp'=>$email_otp
				);

				if($emailsent)
				{
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('resend_email_tan_successfully')
					],REST_Controller::HTTP_OK);
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('resend_email_tan_not_sent')
						], REST_Controller::HTTP_OK);
				}
		}	
	}
	// function ends here for email edit
	
	// function to edit the mobile number
	public function company_edit_mobile_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
				'status' => false,
				'code' => 400,
				'data'=>$this->empty_object,
				'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
				$mobile_no=$this->input->post('mobile_no');
				$phone_code=$this->input->post('phone_code');
				
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$this->lang->load('smstext');
				
				$cond="company_id = '".$user_id."'";
				$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
				$user_app_langauge = $isexist['company_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$mobile_otp=mt_rand(1000,9999);
				
				$data_email=array('company_phone'=>$mobile_no, 'company_phone_code'=>$phone_code ,'mobile_otp' => $mobile_otp);
				
				$new_condition="company_id='".$user_id."'";
				$update_mobile=update_table('jp_companies',$new_condition,$data_email);
				
				$full_mobile = $phone_code.$mobile_no;
				$sms_text = $this->lang->line('sms_otp');					
													
				$final_sms_text = sprintf($sms_text, $mobile_otp);
				
				$this->load->helper('sms_helper');
				$sms_status = send_sms($full_mobile, $final_sms_text);
				
				$handle_status = explode('*#',$sms_status);
				
				if($handle_status[0])
				{
					$data=array(
						'mobile_otp'=>$mobile_otp
					);
			
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('resend_mobile_sent')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$this->empty_object,
							'message' =>$handle_status[1]
						], REST_Controller::HTTP_OK);
				}
		}		
	}
	// function ends here for mobile number
	
	// function to send the mobile verify otp
	public function verify_company_mobile_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		//$this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required');
		//$this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
				'status' => false,
				'code' => 400,
				'data'=>$this->empty_object,
				'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
							
				$user_data = jwt_helper::decode($user_token);
				
				$user_id = $user_data->userId;
				
				$checker_data=array
				(
					'user_id'=>$user_id
				);
				
				$user_checker = $this->companies_model->check_user_mobile($checker_data);
				
				if($user_checker)
				{
					$this->lang->load('smstext');
					
					$mobile_no=$user_checker['company_phone'];
					$phone_code=$user_checker['company_phone_code'];
					
					$full_mobile = $phone_code.$mobile_no;
					$sms_text = $this->lang->line('sms_otp');					
					$mobile_otp=mt_rand(1000,9999);
										
					$final_sms_text = sprintf($sms_text, $mobile_otp);
					
					$this->load->helper('sms_helper');
					$sms_status = send_sms($full_mobile, $final_sms_text);
					
					$handle_status = explode('*#',$sms_status);
					
					// Update User mobile OTP
					$usersms_otp_update=$this->companies_model->companysms_otp_update($user_id, $mobile_otp);
					
					if($handle_status[0])
					{
						$data=array(
							'mobile_otp'=>$mobile_otp
						);
				
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$data,
							'message' =>'Please check mobile for OTP'
						],REST_Controller::HTTP_OK);
						
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$handle_status[1]
							], REST_Controller::HTTP_OK);
					}										
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>'User not exist with this mobile number'
						], REST_Controller::HTTP_OK);
				}				
		}	
	}
	// function for email tan ends here
	
	// function to varify the email tan
	public function mobile_verify_otp_post()
	{
		$this->form_validation->set_rules('mobile_otp', 'Mobile OTP', 'trim|required');
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$mobile_otp=$this->input->post('mobile_otp');
				$user_token=$this->input->post('user_token');
			
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$otp_data=array(
					'user_id'=>$user_id,
					'mobile_otp'=>$mobile_otp
				);
				
				$mobile_verify = $this->companies_model->mobile_otp_verify($otp_data);
				
				if($mobile_verify)
				{
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('mobile_verify_success')
					],REST_Controller::HTTP_OK);
				}else{
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('mobile_tan_is_wrong')
						], REST_Controller::HTTP_OK);
				}
		}	
	}
	// function ends here
	
	public function update_company_profile_post()
	{
		$this->form_validation->set_rules('company_first_name','First Name', 'trim|required');
		$this->form_validation->set_rules('company_last_name', 'Last Name', 'trim|required');
		
		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		
		$this->form_validation->set_rules('company_country', 'Country', 'trim|required');
		$this->form_validation->set_rules('company_state', 'State', 'trim|required');
		$this->form_validation->set_rules('company_city', 'City', 'trim|required');
		
		//$this->form_validation->set_rules('company_phone', 'Mobile Number', 'trim|required');
		
		$this->form_validation->set_rules('company_address', 'Address', 'trim|required');
		$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
		
		$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
		$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$user_token=$this->input->post('user_token');		
			$user_data = jwt_helper::decode($user_token);
			$user_id = $user_data->userId;
			
			$cond="company_id = '$user_id'";
			$isexist=exists_indb('jp_companies',$cond,'company_logo,company_app_language');
			
			$user_app_langauge = $isexist['company_app_language'];
			$this->lang->load('authtext', $user_app_langauge);
			
			$company_first_name=$this->input->post('company_first_name');
			$company_last_name=$this->input->post('company_last_name');
			
			$company_name=$this->input->post('company_name');
			
			//$company_phone=$this->input->post('company_phone');				
			//$company_phone_code = $this->input->post('company_phone_code');
			
			$company_address = $this->input->post('company_address');
			$postal_code = $this->input->post('postal_code');
			
			$company_latitude = $this->input->post('latitude');
			$company_longitude = $this->input->post('longitude');
							
			$company_country=$this->input->post('company_country');
			$company_state=$this->input->post('company_state');				
			$company_city=$this->input->post('company_city');
			
			$company_data = array
			(
				'company_first_name'=>$company_first_name,
				'company_last_name'=>$company_last_name,
				'company_name'=>$company_name,
				'company_country'=>$company_country,
				'company_state'=>$company_state,
				'company_city'=>$company_city,
				'company_pincode'=>$postal_code,
				'company_address'=>$company_address,
				'company_latitude'=>$company_latitude,
				'company_longitude'=>$company_longitude,
				'company_updated_at' => date('Y-m-d H:i:s')
			);
				
			$data = $this->security->xss_clean($company_data);				
			$result = update_table('jp_companies',$cond,$data);
			if($result)
			{
				$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$this->empty_object,
				'message' =>$this->lang->line('update_success')
				],REST_Controller::HTTP_OK);
				
			}else{
					$this->response([
					'status' => true,
					'code' => 401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('update_fail')
					], REST_Controller::HTTP_OK);
			}			
		}
	}
	
	// function to update the company logo image
	public function update_company_logo_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$user_token=$this->input->post('user_token');		
			$user_data = jwt_helper::decode($user_token);
			$user_id = $user_data->userId;
			
			$cond="company_id = '$user_id'";
			$isexist=exists_indb('jp_companies',$cond,'company_logo,company_app_language');
			
			$user_app_langauge = $isexist['company_app_language'];
			$this->lang->load('authtext', $user_app_langauge);
			
			if(!empty($_FILES['company_logo']['size']))
			{
				$config['upload_path'] = './public/images/company_docs';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = 5000;
				$config['max_width'] = 5000;
				$config['max_height'] = 5000;
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config);
				
				if(!$this->upload->do_upload('company_logo'))
				{
					$error = $this->upload->display_errors();
										
					$this->response([
						'status' => false,
						'code' => 400,
						'data'=>$this->empty_object,
						'message' =>$error	
					],REST_Controller::HTTP_OK);
					
				}else{
					$company_logo = $this->upload->data('file_name');
				}
			}else{
					$company_logo = $isexist['company_logo'];
			}
			
            $user_data = array
				(
					'company_logo'=>$company_logo,
					'company_updated_at' =>date('Y-m-d H:i:s')
				);
				
				$data = $this->security->xss_clean($user_data);				
				$result = update_table('jp_companies',$cond,$data);
				if($result)
				{
					$image_path = base_url().'public/images/company_docs/'.$company_logo;
					
					$image_data=array('company_logo'=>$image_path);
					
					$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$image_data,
					'message' =>$this->lang->line('update_success')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
						'status' => true,
						'code' => 401,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_fail')
						], REST_Controller::HTTP_OK);
				}			
		}
	}
	// function ends here for user profile image
	
	// function to add and edit the company info
	public function add_update_company_info_post()
	{
		$this->form_validation->set_rules('industry_id','Industry Name', 'trim|required');
		$this->form_validation->set_rules('company_employees', 'Company Size', 'trim|required');		
		$this->form_validation->set_rules('company_website', 'Company URL', 'trim|required');
		$this->form_validation->set_rules('company_established', 'Company Founded Year', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$user_token=$this->input->post('user_token');		
			$user_data = jwt_helper::decode($user_token);
			$user_id = $user_data->userId;
			
			$cond="company_id = '$user_id'";
			$isexist=exists_indb('jp_companies',$cond,'company_logo,company_app_language');
			
			$user_app_langauge = $isexist['company_app_language'];
			$this->lang->load('authtext', $user_app_langauge);
			
			$industry_id=$this->input->post('industry_id');
			$company_employees=$this->input->post('company_employees');	
			$company_established=$this->input->post('company_established');
			$company_website=$this->input->post('company_website');
			
			$company_data = array
			(
				'industry_id'=>$industry_id,
				'company_employees'=>$company_employees,
				'company_established'=>$company_established,
				'company_website'=>$company_website,
				'company_updated_at' => date('Y-m-d H:i:s')
			);
				
			$data = $this->security->xss_clean($company_data);				
			$result = update_table('jp_companies',$cond,$data);
			if($result)
			{
				$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$this->empty_object,
				'message' =>$this->lang->line('update_success')
				],REST_Controller::HTTP_OK);
				
			}else{
					$this->response([
					'status' => true,
					'code' => 401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('update_fail')
					], REST_Controller::HTTP_OK);
			}			
		}
	}
	// function ends here
	
	// function to upload the company video profile
	public function company_add_edit_video_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="company_id = '$user_id'";
				$isexist=exists_indb('jp_companies',$cond,'company_logo,company_app_language');
				
				$user_app_langauge = $isexist['company_app_language'];
				$this->lang->load('authtext', $user_app_langauge);	
					
				$video_id = $this->input->post('video_id');
				
				if(!empty($_FILES['user_video']['size']))
				{
					$config['upload_path'] = './public/images/company_docs';
					$config['allowed_types'] = 'mp4|ogg|wmv|mov';
					$config['max_size'] = 8500;
					//$config['max_width'] = 1500;
					//$config['max_height'] = 768;
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload', $config);
					
					$orignial_file = $_FILES['user_video']['name'];
					$video_title = explode('.',$orignial_file);
					
					if(!$this->upload->do_upload('user_video'))
					{
						$error = $this->upload->display_errors();
											
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$error
						],REST_Controller::HTTP_OK);
						
					}else{
						$user_video_name = $this->upload->data('file_name');
					}
					
					$video_data=array(
						'company_video'=>$user_video_name
					);
					
					$result = update_table('jp_companies',$cond,$video_data);
					
					if($result)
					{
						$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_success')
						],REST_Controller::HTTP_OK);
						
					}else{
							$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('update_fail')
							], REST_Controller::HTTP_OK);
					}
					
				}else{
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('video_not_found')
						],REST_Controller::HTTP_OK);	
				}
		}
	}
	// function ends here for company video
	
	public function company_home_get()
	{
		$posted_jobs = array();
		
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$job_data = get_all_data('jp_job_posts',$cond,'*');
		
		if(!empty($job_data))
		{
			foreach($job_data as $prefer_jobs)
			{
				
				$company_logo = base_url().'public/images/company_app_no_logo.png';
				if($prefer_jobs['is_own_job']=='Y')
				{
					$logo_name = getSingleValue('jp_companies','company_logo','company_id',$prefer_jobs['company_id']);
					
					if(!empty($logo_name))
					{
						$company_logo = base_url().'public/images/company_docs/'.$logo_name;	
					}
					
				}
				
				$job_experience='';
				if(!empty($prefer_jobs['job_experience_id']))
				{
					$job_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$prefer_jobs['job_experience_id']);
				}
				
				$country_name='';
				if(!empty($prefer_jobs['job_country_id']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$prefer_jobs['job_country_id']);
				}
				
				$state_name='';
				if(!empty($prefer_jobs['job_state_id']))
				{
					$state_name = getSingleValue('jp_states','name','id',$prefer_jobs['job_state_id']);
				}
				
				$city_name='';
				if(!empty($prefer_jobs['job_city_id']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$prefer_jobs['job_city_id']);
				}
				
				$skill_names='';
				if(!empty($prefer_jobs['job_skill_id']))
				{
					$skill_ids = explode(',',$prefer_jobs['job_skill_id']);
					
					$column_name = 'job_skill_id';
					$column_array = $skill_ids;
					$skills_data = get_all_wherein_data('jp_job_skills',$column_name,$column_array,'job_skill_name');
					
					if(!empty($skills_data))
					{
						$db_skill_names=array();
						foreach($skills_data as $sd)
						{
							$db_skill_names[] = $sd['job_skill_name'];
						}
						
						$skill_names =implode(',',$db_skill_names);
					}
				}
				
				$data[] = array
				(
					'job_post_id'=>$prefer_jobs['job_post_id'],
					'company_name'=>$prefer_jobs['company_name'],
					'company_logo'=>$company_logo,
					'job_title'=>$prefer_jobs['job_title'],
					'job_experience'=>$job_experience,
					'job_city'=>$city_name,
					'job_state'=>$state_name,
					'job_country'=>$country_name,
					'job_skills'=>$skill_names					
				);
			}
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('no_data_found')
				], REST_Controller::HTTP_OK);
		}		
	}
	
	// function to list the posted jobs
	public function company_posted_jobs_get()
	{
		$active_jobs = array();
		$inactive_jobs = array();
		$closed_jobs = array();
		
		$total_jobs=0;
		$total_active_jobs=0;
		$total_inactive_jobs=0;
		$total_closed_jobs=0;
		
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$job_data = get_all_data('jp_job_posts',$cond,'*');
		
		if(!empty($job_data))
		{
			foreach($job_data as $prefer_jobs)
			{
				
				$company_logo = base_url().'public/images/company_app_no_logo.png';
				if($prefer_jobs['is_own_job']=='Y')
				{
					$logo_name = getSingleValue('jp_companies','company_logo','company_id',$prefer_jobs['company_id']);
					
					if(!empty($logo_name))
					{
						$company_logo = base_url().'public/images/company_docs/'.$logo_name;	
					}
					
				}
				
				$job_experience='';
				if(!empty($prefer_jobs['job_experience_id']))
				{
					$job_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$prefer_jobs['job_experience_id']);
				}
				
				$country_name='';
				if(!empty($prefer_jobs['job_country_id']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$prefer_jobs['job_country_id']);
				}
				
				$state_name='';
				if(!empty($prefer_jobs['job_state_id']))
				{
					$state_name = getSingleValue('jp_states','name','id',$prefer_jobs['job_state_id']);
				}
				
				$city_name='';
				if(!empty($prefer_jobs['job_city_id']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$prefer_jobs['job_city_id']);
				}
				
				$skill_names='';
				if(!empty($prefer_jobs['job_skill_id']))
				{
					$skill_ids = explode(',',$prefer_jobs['job_skill_id']);
					
					$column_name = 'job_skill_id';
					$column_array = $skill_ids;
					$skills_data = get_all_wherein_data('jp_job_skills',$column_name,$column_array,'job_skill_name');
					
					if(!empty($skills_data))
					{
						$db_skill_names=array();
						foreach($skills_data as $sd)
						{
							$db_skill_names[] = $sd['job_skill_name'];
						}
						
						$skill_names =implode(',',$db_skill_names);
					}
				}
				
				$job_date=explode(' ',$prefer_jobs['created_at']);
				$job_post_date=$job_date[0];
				
				if($prefer_jobs['job_status']=='Y')
				{
					$active_jobs[] = array
					(
						'job_post_id'=>$prefer_jobs['job_post_id'],
						'company_name'=>$prefer_jobs['company_name'],
						'company_logo'=>$company_logo,
						'job_title'=>$prefer_jobs['job_title'],
						'job_experience'=>$job_experience,
						'job_city'=>$city_name,
						'job_state'=>$state_name,
						'job_country'=>$country_name,
						'job_skills'=>$skill_names,
						'job_post_date'=>$job_post_date,
						'job_status'=>$prefer_jobs['job_status'],
						'is_hiring_closed'=>$prefer_jobs['is_hiring_closed']
					);
					
					$total_active_jobs++;
				}
				
				if($prefer_jobs['job_status']=='N')
				{
					$inactive_jobs[] = array
					(
						'job_post_id'=>$prefer_jobs['job_post_id'],
						'company_name'=>$prefer_jobs['company_name'],
						'company_logo'=>$company_logo,
						'job_title'=>$prefer_jobs['job_title'],
						'job_experience'=>$job_experience,
						'job_city'=>$city_name,
						'job_state'=>$state_name,
						'job_country'=>$country_name,
						'job_skills'=>$skill_names,
						'job_post_date'=>$job_post_date,
						'job_status'=>$prefer_jobs['job_status'],
						'is_hiring_closed'=>$prefer_jobs['is_hiring_closed']	
					);
					
					$total_inactive_jobs++;
				}
				
				if($prefer_jobs['is_hiring_closed']=='Y')
				{
					$closed_jobs[] = array
					(
						'job_post_id'=>$prefer_jobs['job_post_id'],
						'company_name'=>$prefer_jobs['company_name'],
						'company_logo'=>$company_logo,
						'job_title'=>$prefer_jobs['job_title'],
						'job_experience'=>$job_experience,
						'job_city'=>$city_name,
						'job_state'=>$state_name,
						'job_country'=>$country_name,
						'job_skills'=>$skill_names,
						'job_post_date'=>$job_post_date,
						'job_status'=>$prefer_jobs['job_status'],
						'is_hiring_closed'=>$prefer_jobs['is_hiring_closed']	
					);
					
					$total_closed_jobs++;
				}
				
				$total_jobs++;
				
				$data=array(
					'active_jobs'=>$active_jobs,
					'inactive_jobs'=>$inactive_jobs,
					'closed_jobs'=>$closed_jobs,
					'total_active_jobs'=>$total_active_jobs,
					'total_inactive_jobs'=>$total_inactive_jobs,
					'total_closed_jobs'=>$total_closed_jobs,
					'total_jobs'=>$total_jobs
				);
				
			}
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('companyhome_no_data_found')
				], REST_Controller::HTTP_OK);
		}
		
	}
	// function ends here
	
	// function to list the active posted jobs
	public function company_active_posted_jobs_get()
	{
		$active_jobs = array();
				
		$total_jobs=0;
		$total_active_jobs=0;
				
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$job_data = get_all_data('jp_job_posts',$cond,'*');
		
		if(!empty($job_data))
		{
			foreach($job_data as $prefer_jobs)
			{
				
				$company_logo = base_url().'public/images/company_app_no_logo.png';
				if($prefer_jobs['is_own_job']=='Y')
				{
					$logo_name = getSingleValue('jp_companies','company_logo','company_id',$prefer_jobs['company_id']);
					
					if(!empty($logo_name))
					{
						$company_logo = base_url().'public/images/company_docs/'.$logo_name;	
					}
					
				}
				
				$job_experience='';
				if(!empty($prefer_jobs['job_experience_id']))
				{
					$job_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$prefer_jobs['job_experience_id']);
				}
				
				$country_name='';
				if(!empty($prefer_jobs['job_country_id']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$prefer_jobs['job_country_id']);
				}
				
				$state_name='';
				if(!empty($prefer_jobs['job_state_id']))
				{
					$state_name = getSingleValue('jp_states','name','id',$prefer_jobs['job_state_id']);
				}
				
				$city_name='';
				if(!empty($prefer_jobs['job_city_id']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$prefer_jobs['job_city_id']);
				}
				
				$skill_names='';
				if(!empty($prefer_jobs['job_skill_id']))
				{
					$skill_ids = explode(',',$prefer_jobs['job_skill_id']);
					
					$column_name = 'job_skill_id';
					$column_array = $skill_ids;
					$skills_data = get_all_wherein_data('jp_job_skills',$column_name,$column_array,'job_skill_name');
					
					if(!empty($skills_data))
					{
						$db_skill_names=array();
						foreach($skills_data as $sd)
						{
							$db_skill_names[] = $sd['job_skill_name'];
						}
						
						$skill_names =implode(',',$db_skill_names);
					}
				}
				
				$job_date=explode(' ',$prefer_jobs['created_at']);
				$job_post_date=$job_date[0];
				
				if($prefer_jobs['job_status']=='Y')
				{
					$active_jobs[] = array
					(
						'job_post_id'=>$prefer_jobs['job_post_id'],
						'company_name'=>$prefer_jobs['company_name'],
						'company_logo'=>$company_logo,
						'job_title'=>$prefer_jobs['job_title'],
						'job_experience'=>$job_experience,
						'job_city'=>$city_name,
						'job_state'=>$state_name,
						'job_country'=>$country_name,
						'job_skills'=>$skill_names,
						'job_post_date'=>$job_post_date,
						'job_status'=>$prefer_jobs['job_status'],
						'is_hiring_closed'=>$prefer_jobs['is_hiring_closed']
					);
					
					$total_active_jobs++;
				}
				
				$total_jobs++;
				
				$data=array(
					'active_jobs'=>$active_jobs,
					'total_active_jobs'=>$total_active_jobs,
					'total_jobs'=>$total_jobs
				);
				
			}
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('companyactivejobs_no_data_found')
				], REST_Controller::HTTP_OK);
		}
		
	}
	// function ends here
	
	// function to list the Inactive posted jobs
	public function company_inactive_posted_jobs_get()
	{
		$inactive_jobs = array();
				
		$total_jobs=0;
		$total_inactive_jobs=0;
				
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$job_data = get_all_data('jp_job_posts',$cond,'*');
		
		if(!empty($job_data))
		{
			foreach($job_data as $prefer_jobs)
			{
				
				$company_logo = base_url().'public/images/company_app_no_logo.png';
				if($prefer_jobs['is_own_job']=='Y')
				{
					$logo_name = getSingleValue('jp_companies','company_logo','company_id',$prefer_jobs['company_id']);
					
					if(!empty($logo_name))
					{
						$company_logo = base_url().'public/images/company_docs/'.$logo_name;	
					}
					
				}
				
				$job_experience='';
				if(!empty($prefer_jobs['job_experience_id']))
				{
					$job_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$prefer_jobs['job_experience_id']);
				}
				
				$country_name='';
				if(!empty($prefer_jobs['job_country_id']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$prefer_jobs['job_country_id']);
				}
				
				$state_name='';
				if(!empty($prefer_jobs['job_state_id']))
				{
					$state_name = getSingleValue('jp_states','name','id',$prefer_jobs['job_state_id']);
				}
				
				$city_name='';
				if(!empty($prefer_jobs['job_city_id']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$prefer_jobs['job_city_id']);
				}
				
				$skill_names='';
				if(!empty($prefer_jobs['job_skill_id']))
				{
					$skill_ids = explode(',',$prefer_jobs['job_skill_id']);
					
					$column_name = 'job_skill_id';
					$column_array = $skill_ids;
					$skills_data = get_all_wherein_data('jp_job_skills',$column_name,$column_array,'job_skill_name');
					
					if(!empty($skills_data))
					{
						$db_skill_names=array();
						foreach($skills_data as $sd)
						{
							$db_skill_names[] = $sd['job_skill_name'];
						}
						
						$skill_names =implode(',',$db_skill_names);
					}
				}
				
				$job_date=explode(' ',$prefer_jobs['created_at']);
				$job_post_date=$job_date[0];
				
				if($prefer_jobs['job_status']=='N')
				{
					$inactive_jobs[] = array
					(
						'job_post_id'=>$prefer_jobs['job_post_id'],
						'company_name'=>$prefer_jobs['company_name'],
						'company_logo'=>$company_logo,
						'job_title'=>$prefer_jobs['job_title'],
						'job_experience'=>$job_experience,
						'job_city'=>$city_name,
						'job_state'=>$state_name,
						'job_country'=>$country_name,
						'job_skills'=>$skill_names,
						'job_post_date'=>$job_post_date,
						'job_status'=>$prefer_jobs['job_status'],
						'is_hiring_closed'=>$prefer_jobs['is_hiring_closed']	
					);
					
					$total_inactive_jobs++;
				}
				
				$total_jobs++;
				
				$data=array(
					'inactive_jobs'=>$inactive_jobs,
					'total_inactive_jobs'=>$total_inactive_jobs,
					'total_jobs'=>$total_jobs
				);
				
			}
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('companyinactivejobs_no_data_found')
				], REST_Controller::HTTP_OK);
		}
		
	}
	// function ends here
	
	// function to list the closed posted jobs
	public function company_closed_jobs_get()
	{
		$closed_jobs = array();
		
		$total_jobs=0;
		$total_closed_jobs=0;
		
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$job_data = get_all_data('jp_job_posts',$cond,'*');
		
		if(!empty($job_data))
		{
			foreach($job_data as $prefer_jobs)
			{
				
				$company_logo = base_url().'public/images/company_app_no_logo.png';
				if($prefer_jobs['is_own_job']=='Y')
				{
					$logo_name = getSingleValue('jp_companies','company_logo','company_id',$prefer_jobs['company_id']);
					
					if(!empty($logo_name))
					{
						$company_logo = base_url().'public/images/company_docs/'.$logo_name;	
					}
					
				}
				
				$job_experience='';
				if(!empty($prefer_jobs['job_experience_id']))
				{
					$job_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$prefer_jobs['job_experience_id']);
				}
				
				$country_name='';
				if(!empty($prefer_jobs['job_country_id']))
				{
					$country_name = getSingleValue('jp_countries','name','id',$prefer_jobs['job_country_id']);
				}
				
				$state_name='';
				if(!empty($prefer_jobs['job_state_id']))
				{
					$state_name = getSingleValue('jp_states','name','id',$prefer_jobs['job_state_id']);
				}
				
				$city_name='';
				if(!empty($prefer_jobs['job_city_id']))
				{
					$city_name = getSingleValue('jp_cities','name','id',$prefer_jobs['job_city_id']);
				}
				
				$skill_names='';
				if(!empty($prefer_jobs['job_skill_id']))
				{
					$skill_ids = explode(',',$prefer_jobs['job_skill_id']);
					
					$column_name = 'job_skill_id';
					$column_array = $skill_ids;
					$skills_data = get_all_wherein_data('jp_job_skills',$column_name,$column_array,'job_skill_name');
					
					if(!empty($skills_data))
					{
						$db_skill_names=array();
						foreach($skills_data as $sd)
						{
							$db_skill_names[] = $sd['job_skill_name'];
						}
						
						$skill_names =implode(',',$db_skill_names);
					}
				}
				
				$job_date=explode(' ',$prefer_jobs['created_at']);
				$job_post_date=$job_date[0];
				
				if($prefer_jobs['is_hiring_closed']=='Y')
				{
					$closed_jobs[] = array
					(
						'job_post_id'=>$prefer_jobs['job_post_id'],
						'company_name'=>$prefer_jobs['company_name'],
						'company_logo'=>$company_logo,
						'job_title'=>$prefer_jobs['job_title'],
						'job_experience'=>$job_experience,
						'job_city'=>$city_name,
						'job_state'=>$state_name,
						'job_country'=>$country_name,
						'job_skills'=>$skill_names,
						'job_post_date'=>$job_post_date,
						'job_status'=>$prefer_jobs['job_status'],
						'is_hiring_closed'=>$prefer_jobs['is_hiring_closed']	
					);
					
					$total_closed_jobs++;
				}
				
				$total_jobs++;
				
				$data=array(
					'closed_jobs'=>$closed_jobs,
					'total_closed_jobs'=>$total_closed_jobs,
					'total_jobs'=>$total_jobs
				);
				
			}
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('companyclosedjobs_no_data_found')
				], REST_Controller::HTTP_OK);
		}
		
	}
	// function ends here
	
	// function to hide the posted job
	public function company_hide_posted_jobs_post()
	{
		$job_post_id = $this->input->post('job_post_id');
		$job_status = $this->input->post('job_status');
		$hiring_status = $this->input->post('is_hiring_closed');
		
		if(!empty($job_status))
		{
			$cond="job_post_id = '$job_post_id'";
			
			$is_hiring_closed = ($job_status == 'Y')?'N': 'Y';
			$update_data=array
			(
				'job_status'=>$job_status,
				'is_hiring_closed'=>$is_hiring_closed
			);
		}
		
		if(!empty($hiring_status))
		{
			$cond="job_post_id = '$job_post_id'";
			
			$is_job_status = ($hiring_status == 'N')?'Y': 'N';
			$update_data=array
			(
				'is_hiring_closed'=>$hiring_status,
				'job_status'=>$is_job_status
			);
		}		
		$update_token=update_table('jp_job_posts',$cond,$update_data);
		
		if($update_token)
		{
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$this->empty_object,
				'message' =>$this->lang->line('update_success')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status'=> false,
					'code' => 200,
					'data'=>$this->empty_object,
					'message'=>$this->lang->line('update_fail')
				],REST_Controller::HTTP_OK);
		}
	}
	// function ends here
	
	// function to list the users
	public function company_users_listing_get()
	{
		$users_listing = array();
		
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
		
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		// code to get the company industry ids
		$company_industries = $this->companies_model->get_distinct_company_industry($user_id);
		//print_r($company_industries);
		
		// code for notification count
		$notification_counter=0;
		$notification_count_data = $this->companies_model->get_notifications_count($user_id);
		if(!empty($notification_count_data))
		{
			$notification_counter=$notification_count_data;
										
		}	
		// code for notification count ends here
		
		// code ends here for industry 
		if(!empty($company_industries))
		{
			foreach($company_industries as $comp_indus)
			{
				$industries[] = intval($comp_indus['job_industry_id']);
			}
			
			$prefer_user_ids = $this->companies_model->get_prefer_users($industries);
			
			//print_r($prefer_user_ids);
			
			if(!empty($prefer_user_ids))
			{
				$users_data = $this->companies_model->get_users($prefer_user_ids);	
			}else{
				
				$users_cond="user_status = 'Y'";
				$users_data = get_all_data('jp_users',$users_cond,'*');
			}
			
		}else{
				$users_cond="user_status = 'Y'";
				$users_data = get_all_data('jp_users',$users_cond,'*');
		}
		
		// code to get the users who accepted the swipe
		$swipe_users = $this->companies_model->swipe_accepted_users($user_id);
		//echo "<pre/>";
		//print_r($swipe_users);
		$company_rejected_users =array();
		if(!empty($swipe_users))
		{
			foreach($swipe_users as $swp_user)
			{
				if($swp_user['company_id']==$user_id)
				{
					if($swp_user['company_status']=='R')
					{
						$company_rejected_users[]=$swp_user['user_id'];					
					}
					
					if($swp_user['company_status']=='A')
					{
						$company_rejected_users[]=$swp_user['user_id'];					
					}
				}			
			}
		}
		
		//print_r($company_rejected_users);
		
		// code for users swipe ends here
		
		// code for checking the package expire and job listing limit
		$left_package_days=0;
		$left_posted_jobs = 0;
		$left_swipes =0;
		
		$package_data = get_row_data('jp_company_job_packages',$cond,'*');
		$package_enddate = $package_data['package_end'];
		
		if($package_data['is_free']=='Y')
		{
			/*$now = time(); // or your date as well
			$your_date = strtotime($package_enddate);
			$datediff = $your_date - $now;
			if($datediff <=0)
			{
				$left_package_days=0;	
			}else{
				$left_package_days=round($datediff / (60 * 60 * 24));			
			}*/
			
			$left_package_days =1;
			
			$current_month = date('m');
			$today_date = date('Y-m-d');
			//echo $current_month;
			
			if($current_month!=$package_data['listing_used_month'])
			{
				$left_posted_jobs = (int) $package_data['package_listings'];
			}
			
			$chk_swipe_cond ="company_id = '$user_id' AND swipe_date='$today_date'";
			$user_swipe_data = get_row_data('jp_company_swipes',$chk_swipe_cond,'*');
			
			if(!empty($user_swipe_data))
			{
				$left_swipes = $user_swipe_data['total_swipes']-$user_swipe_data['used_swipes'];
				if($left_swipes<0)
				{
					$left_swipes=0;
				}
			}
			
			
			/*$left_posted_jobs = $package_data['package_listings'] - $package_data['used_listings'];
			if($left_posted_jobs<0)
			{
				$left_posted_jobs=0;
			}*/
		}else{
				$left_package_days=1;
				
				if($package_data['package_listings']==0)
				{
					$left_posted_jobs =1;
				}else{
						$current_month = date('m');
						
						if($current_month!=$package_data['listing_used_month'])
						{
							$left_posted_jobs = (int) $package_data['package_listings'];
						}					
				}
				
				if($package_data['free_swipes']==0)
				{
					$left_swipes=1;
				}else{
						$today_date = date('Y-m-d');
						
						$chk_swipe_cond ="company_id = '$user_id' AND swipe_date='$today_date'";
						$user_swipe_data = get_row_data('jp_company_swipes',$chk_swipe_cond,'*');
						
						if(!empty($user_swipe_data))
						{
							$left_swipes = $user_swipe_data['total_swipes']-$user_swipe_data['used_swipes'];
							if($left_swipes<0)
							{
								$left_swipes=0;
							}
						}else{
								$left_swipes = (int) $package_data['free_swipes'];
						}					
				}				
		}
		
		
		// code ends here for package and job listing limit
		
		if(!empty($users_data))
		{
			foreach($users_data as $users)
			{
				if(!in_array($users['user_id'], $company_rejected_users))
				{
					$match_id= '';
					$company_match_status='';
					$user_match_status='';
					$job_id='';
				
					if(!empty($swipe_users))
					{
						foreach($swipe_users as $swp_user)
						{
							if($swp_user['company_id']==$user_id)
							{
								if($users['user_id']==$swp_user['user_id'])
								{
									$match_id = $swp_user['match_id'];
									$company_match_status = $swp_user['company_status'];
									$user_match_status = $swp_user['user_status'];
									$job_id = $swp_user['job_id'];
								}
							}							
						}
					}
					
					$user_profile = base_url().'public/dist/img/avatar5.png';
					if(!empty($users['profile_image']))
					{
						$user_profile = base_url().'public/images/user_docs/'.$users['profile_image'];
					}
					
					$country_name='';
					if(!empty($users['country']))
					{
						$country_name = getSingleValue('jp_countries','name','id',$users['country']);
					}
					
					$state_name='';
					if(!empty($users['state']))
					{
						$state_name = getSingleValue('jp_states','name','id',$users['state']);
					}
					
					$city_name='';
					if(!empty($users['city']))
					{
						$city_name = getSingleValue('jp_cities','name','id',$users['city']);
					}
					
					$gender_name='';
					if(!empty($users['gender_id']))
					{
						$gender_name = getSingleValue('jp_genders','gender_name','gender_id',$users['gender_id']);
					}
					
					$user_dob='';
					if(!empty($users['dob']))
					{
						$user_dob = $users['dob'];
					}
					
					$user_age='';
					if(!empty($users['dob']))
					{
						$from = new DateTime($users['dob']);
						$to = new DateTime('today');
						$user_age = $from->diff($to)->y;
					}
					
					$work_experience='';
					if(!empty($users['experience_id']))
					{
						$work_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$users['experience_id']);
					}
					
					$carrier_objective='';
					if(!empty($users['profile_summary']))
					{
						$carrier_objective = $users['profile_summary'];
					}
					
					$skills_data = $this->users_model->users_skills($users['user_id']);
					$user_key_skills=array();
					if(!empty($skills_data))
					{
						foreach($skills_data as $skills)
						{
							$skill_name='';
							if(!empty($skills['skill_id']))
							{
								$skill_name = getSingleValue('jp_job_skills','job_skill_name','job_skill_id',$skills['skill_id']);
							}
							$user_key_skills[]=$skill_name;
						}
					}
					
					$education_data = $this->users_model->education_data($users['user_id']);
					$degree_name='';
					if(!empty($education_data))
					{
						foreach($education_data as $education)
						{						
							if(!empty($education['degree_type_id']))
							{
								$degree_name = getSingleValue('jp_degree_type','job_degree_type_name','job_degree_type_id',$education['degree_type_id']);
							}						
						}
					}
					
					$users_listing[] = array
					(
						'user_id' => $users['user_id'],
						'first_name'=>$users['first_name'],
						'middle_name'=>$users['middle_name'],
						'last_name'=>$users['last_name'],
						'user_profile'=>$user_profile,
						'email'=>$users['email'],
						'degree_name'=>$degree_name,
						'skill_name'=>$user_key_skills,
						'mobile_no' =>$users['mobile_no'],
						'phone_code'=>$users['phone_code'],
						'country'=>$country_name,
						'state'=>$state_name,
						'city'=>$city_name,
						'user_age'=>$user_age,
						'user_dob'=>$user_dob,
						'gender'=>$gender_name,
						'match_id'=>$match_id,
						'company_match_status'=>$company_match_status,
						'user_match_status'=>$user_match_status,
						'job_id'=>$job_id
					);
				}									
			}
			
			$data=array(
				'users_listing'=>$users_listing,
				'company_notification_counter'=>$notification_counter,
				'left_package_days'=>$left_package_days,
				'left_posted_jobs'=>$left_posted_jobs,
				'left_swipes'=>$left_swipes
			);
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('companyhomeusers_no_data_found')
				], REST_Controller::HTTP_OK);
		}
	}
	// function ends here
	
	// function to show the job post data
	public function single_user_data_get()
	{
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		// code to fetch the user details
		
		$job_user_id = $this->input->get('job_user_id');
		
		// code to get the latest user match
		$match_id= '';
		$company_match_status='';
		$user_match_status='';
		
		$swipe_users = $this->companies_model->user_company_match($user_id,$job_user_id);
		if(!empty($swipe_users))
		{
			$match_id = $swipe_users['match_id'];
			$company_match_status = $swipe_users['company_status'];
			$user_match_status = $swipe_users['user_status'];
		}
		// code ends here for latest match
		
		$job_user_cond="user_id = '$job_user_id'";
		$row_user_data = get_row_data('jp_users',$job_user_cond,'*');
		if(!empty($row_user_data))
		{
			$user_profile = base_url().'public/dist/img/avatar5.png';
			if(!empty($row_user_data['profile_image']))
			{
				$user_profile = base_url().'public/images/user_docs/'.$row_user_data['profile_image'];
			}
			
			$country_name='';
			if(!empty($row_user_data['country']))
			{
				$country_name = getSingleValue('jp_countries','name','id',$row_user_data['country']);
			}
			
			$state_name='';
			if(!empty($row_user_data['state']))
			{
				$state_name = getSingleValue('jp_states','name','id',$row_user_data['state']);
			}
			
			$city_name='';
			if(!empty($row_user_data['city']))
			{
				$city_name = getSingleValue('jp_cities','name','id',$row_user_data['city']);
			}
			
			$gender_name='';
			if(!empty($row_user_data['gender_id']))
			{
				$gender_name = getSingleValue('jp_genders','gender_name','gender_id',$row_user_data['gender_id']);
			}
			
			$user_dob='';
			if(!empty($row_user_data['dob']))
			{
				$user_dob = $row_user_data['dob'];
			}
			
			$user_age='';
			if(!empty($row_user_data['dob']))
			{
				$from = new DateTime($row_user_data['dob']);
				$to = new DateTime('today');
				$user_age = $from->diff($to)->y;
			}
			
			$work_experience='';
			if(!empty($row_user_data['experience_id']))
			{
				$work_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$row_user_data['experience_id']);
			}
			
						
			$skills_data = $this->users_model->users_skills($row_user_data['user_id']);
			$user_key_skills=array();
			if(!empty($skills_data))
			{
				foreach($skills_data as $skills)
				{
					$skill_name='';
					if(!empty($skills['skill_id']))
					{
						$skill_name = getSingleValue('jp_job_skills','job_skill_name','job_skill_id',$skills['skill_id']);
					}
					$user_key_skills[]=$skill_name;
				}
			}
			
			$education_data = $this->users_model->education_data($row_user_data['user_id']);
			$degree_name='';
			if(!empty($education_data))
			{
				foreach($education_data as $education)
				{
					if(!empty($education['degree_type_id']))
					{
						$degree_name = getSingleValue('jp_degree_type','job_degree_type_name','job_degree_type_id',$education['degree_type_id']);
					}						
				}
			}
			
			$carrier_objective='';
			if(!empty($row_user_data['profile_summary']))
			{
				$carrier_objective = $row_user_data['profile_summary'];
			}
			
			$user_work_experience=array();
			$work_experience_data = $this->users_model->work_experience($row_user_data['user_id']);
			if(!empty($work_experience_data))
			{
				foreach($work_experience_data as $work)
				{
					$experience_title='';
					if(!empty($work['experience_title']))
					{
						$experience_title = getSingleValue('jp_functional_areas','functional_name','functional_id',$work['experience_title']);
					}
					
					$work_from_datefmt= date("F Y", strtotime($work['start_date']));
					$work_to_datefmt= date("F Y", strtotime($work['end_date']));
					
					$user_work_experience[]=array
					(	
						'user_experience_id'=>$work['user_experience_id'],
						'company_name'=>$work['company_name'],
						'experience_title'=>$experience_title,
						'work_from'=>$work_from_datefmt,
						'work_to'=>$work_to_datefmt,
						'currently_working'=>$work['currently_working'],
						'description'=>$work['experience_description']
					);
				}
			}
			
			$user_eductaion=array();
			$education_data = $this->users_model->education_data($row_user_data['user_id']);
			if(!empty($education_data))
			{
				foreach($education_data as $education)
				{
					$university_name='';
					if(!empty($education['college_name']))
					{
						$university_name = getSingleValue('jp_university_college','university_college_name','university_college_id',$education['college_name']);
					}
					
					$degree_level='';
					if(!empty($education['degree_level_id']))
					{
						$degree_level = getSingleValue('jp_degree_level','job_degree_level_name','job_degree_level_id',$education['degree_level_id']);
					}
					
					$degree_name='';
					if(!empty($education['degree_type_id']))
					{
						$degree_name = getSingleValue('jp_degree_type','job_degree_type_name','job_degree_type_id',$education['degree_type_id']);
					}
					
					$start_date='';
					if(!empty($education['start_date']))
					{
						$start_date = $education['start_date'];
					}
					
					$end_date='';
					if(!empty($education['end_date']))
					{
						$end_date = $education['end_date'];
					}
					
					$education_from_datefmt= date("F Y", strtotime($start_date));
					$education_to_datefmt= date("F Y", strtotime($end_date));
					
					$user_eductaion[]=array
					(
						'user_education_id'=>$education['user_education_id'],
						'college_university_name'=>$university_name,
						'degree_level'=>$degree_level,
						'degree_name'=>$degree_name,
						'from'=>$education_from_datefmt,
						'to'=>$education_to_datefmt,
						'currently_pursuing'=>$education['currently_pursuing']
					);
				}
			}
			
			$user_preferences=array();
			$prefrences_data = $this->users_model->prefrences_data($row_user_data['user_id']);
			if(!empty($prefrences_data))
			{
				foreach($prefrences_data as $preference)
				{
					$location_ids=explode(',',$preference['location_ids']);
					
					$preference_locations=array();
					if(!empty($location_ids))
					{
						foreach($location_ids as $loc_id)
						{
							$location_name = getSingleValue('jp_cities','name','id',$loc_id);
							$preference_locations[]=array
							(
								'location_id'=>$loc_id,
								'location_name'=>$location_name
							);
						}
					}
					
					$industry_ids=explode(',',$preference['industry_ids']);
					
					$preference_industry=array();
					if(!empty($industry_ids))
					{
						foreach($industry_ids as $indus_id)
						{
							$industry_name = getSingleValue('jp_industries','industry_name','industry_id',$indus_id);
							$preference_industry[]=array
							(
								'industry_id'=>$indus_id,
								'industry_name'=>$industry_name
							);
						}
					}
					
					$expected_salary_number = '';
					$expected_salary_words ='';
					$salary_array =array();
					if(!empty($preference['expected_salary']))
					{
						$expected_salary_words = $this->convert_number_to_words($preference['expected_salary']+0);
						$salary_array=array
						(
							'expected_salary_number'=>$preference['expected_salary']+0,
							'expected_salary_words'=>$expected_salary_words
						);						
					}
					
					$user_preferences[]=array
					(
						'preference_id'=>$preference['preference_id'],
						'location_data'=>$preference_locations,
						'industry_data'=>$preference_industry,
						'expected_salary'=>$salary_array
					);
				}
			}
			
			$user_languages=array();
			$language_data = $this->users_model->language_data($row_user_data['user_id']);
			if(!empty($language_data))
			{
				foreach($language_data as $language)
				{
					$language_name='';
					if(!empty($language['language_id']))
					{
						$language_name = getSingleValue('jp_languages','language_name','language_id',$language['language_id']);
					}
															
					$user_languages[]=array
					(
						'language_id'=>$language['language_id'],
						'language_name'=>$language_name
					);
				}
			}
			
			$resumes_data = $this->users_model->resumes_data($job_user_id);
			//$user_resumes=array();
			if(!empty($resumes_data))
			{
				foreach($resumes_data as $resumes)
				{
					$user_resumes=array
					(
						'cv_id'=>$resumes['user_cv_id'],
						'cv_title'=>$resumes['user_cv_title'],
						'cv_file'=>$resumes['orignal_cv_name'],
						'cv_file_link'=>base_url().'public/images/user_docs/'.$resumes['user_cv_name']
					);
				}
			}else{
					$user_resumes = $this->empty_object;
			}
			
			$video_data = $this->users_model->video_data($row_user_data['user_id']);
			if(!empty($video_data))
			{
				foreach($video_data as $video)
				{
					$user_videos=array
					(
						'video_id'=>$video['video_id'],
						'video_title'=>$video['video_title'],
						'video_file'=>$video['orignal_video_name'],
						'video_file_link'=>base_url().'public/images/user_docs/'.$video['video_name']
					);
				}
			}else{
					$user_videos = $this->empty_object;
			}
					
			$job_user_data = array
			(
				'user_id' => $row_user_data['user_id'],
				'first_name'=>$row_user_data['first_name'],
				'middle_name'=>$row_user_data['middle_name'],
				'last_name'=>$row_user_data['last_name'],
				'user_profile'=>$user_profile,
				'email'=>$row_user_data['email'],
				'degree_name'=>$degree_name,
				'skill_name'=>$user_key_skills,
				'mobile_no' =>$row_user_data['mobile_no'],
				'phone_code'=>$row_user_data['phone_code'],
				'country'=>$country_name,
				'state'=>$state_name,
				'city'=>$city_name,
				'carrier_objective'=>$carrier_objective,
				'work_experience'=>$work_experience,
				'user_age'=>$user_age,
				'user_dob'=>$user_dob,
				'gender'=>$gender_name,
				'match_id'=>$match_id,
				'company_match_status'=>$company_match_status,
				'user_match_status'=>$user_match_status,
				'user_resumes'=>$user_resumes,
				'user_work_experience'=>$user_work_experience,
				'user_eductaion'=>$user_eductaion,
				'user_preferences'=>$user_preferences,
				'user_languages'=>$user_languages,
				'user_videos'=>$user_videos
			);
			
			$data=array
			(
			  'job_user_data'=>$job_user_data
			);
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message'=>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
				'status' => true,
				'code' =>401,
				'data'=>$this->empty_object,
				'message' =>$this->lang->line('companyhomeusers_no_data_found')
			], REST_Controller::HTTP_OK);
		}
		// code ends here
		
	}
	// function ends here
	
	// function to send reset password link
	public function company_forgot_password_post()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))],REST_Controller::HTTP_OK);	
		}else
		{
				$email = $this->input->post('email');
				
				$cond="company_email = '$email'";
				$isexist=exists_indb('jp_companies',$cond,'company_email,company_name,company_app_language');
				
				if($isexist)
				{
					$user_name = $isexist['company_name'];
					$token=RandomStringGenerator(30);
					
					$user_app_langauge = $isexist['company_app_language'];
					$this->lang->load('authtext', $user_app_langauge);
					
					$mail_data['result']=array
					(
						"token" =>$token,
						"name" =>$user_name
					);
					
					$update_data=array
					(
						'forgot_password_token'=>$token,
						'p_token_expires_at' => date('Y-m-d H:i:s', strtotime('24 hours'))
					);
								
					$update_token=update_table('jp_companies',$cond,$update_data);
					
					if($update_token)
					{
						$to=$email;
						$subject=$this->lang->line('forgot_email_subject');
						$attachment='';
						$cc='';
						$body=$this->load->view('email_template/reset_company_password_link',$mail_data,true);
						$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
						
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('forgot_email_sent')
							],REST_Controller::HTTP_OK);
					}else{
						$this->response([
						'status'=> false,
						'code' => 200,
						'data'=>$this->empty_object,
						'message'=>$this->lang->line('forgot_email_not_sent')
						],REST_Controller::HTTP_OK);
					}
					
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('forgot_email_not_found')
						], REST_Controller::HTTP_OK);
				}
		}
	}
	// function ends here
	
	// function for company swipe the user
	public function company_user_swipe_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('company_status', 'Company Status', 'trim|required');
		$this->form_validation->set_rules('user_id', 'User ID', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$company_id = $user_data->userId;
				
				$cond="company_id = '$company_id'";
				$isexist=exists_indb('jp_companies',$cond,'company_name,company_app_language');
				
				$user_app_langauge = $isexist['company_app_language'];
				$this->lang->load('authtext', $user_app_langauge);	
					
				//$job_post_id = $this->input->post('job_post_id');
				$user_id = $this->input->post('user_id');
				$company_status = $this->input->post('company_status');
				$match_id = $this->input->post('match_id');
				
				// check and update the company swipes
				// company package info
					$user_package_data = get_row_data('jp_company_job_packages',$cond,'company_package_id,free_swipes,used_swipes');
					
					$package_id =$user_package_data['company_package_id'];
					$pack_condi ="company_package_id = '$package_id'";
					if(!empty($user_package_data['free_swipes']))
					{	
						$today_date = date('Y-m-d');
						$chk_swipe_cond ="company_id = '$company_id' AND swipe_date='$today_date'";
						$user_swipe_data = get_row_data('jp_company_swipes',$chk_swipe_cond,'used_swipes');
						
						if(empty($user_swipe_data))
						{
							$data=array(
										'company_id'=>$company_id,
										'swipe_date'=>date('Y-m-d'),
										'total_swipes'=>$user_package_data['free_swipes'],
										'used_swipes'=>1
							);
							insertdata('jp_company_swipes',$data);
						}else{
								$existing_swipes = $user_swipe_data['used_swipes'];
								$new_swipe = $existing_swipes+1;
								
								$data=array('used_swipes'=>$new_swipe);
								update_table('jp_company_swipes',$chk_swipe_cond,$data);
						}
				
						
					}else{
							$pack_existing_swipes = $user_package_data['used_swipes'];
							$pack_new_swipe = $pack_existing_swipes+1;							
							$pack_swipe_data=array('used_swipes'=>$pack_new_swipe);
							
							update_table('jp_company_job_packages',$pack_condi,$pack_swipe_data);
					}
					
				// package info ends here
				
					/*$user_swipe_data = get_row_data('jp_company_swipes',$cond,'total_swipes');
					
					if(empty($user_swipe_data))
					{
						$data=array('company_id'=>$company_id,'total_swipes'=>1);
						insertdata('jp_company_swipes',$data);
					}else{
							$existing_swipes = $user_swipe_data['total_swipes'];
							$new_swipe = $existing_swipes+1;
							
							$data=array('total_swipes'=>$new_swipe);
							update_table('jp_company_swipes',$cond,$data);
					}*/
				// check ends here
				
				// code to check if user swipe for the same match id
					$swipe_data=array(
					   'company_id'=>$company_id,
					   'user_id'=>$user_id,
					   'company_status'=>$company_status,
					   'company_date'=>date('Y-m-d h:i:s')
					);	
					
					$job_post_id='';
					$user_status='';
					if(empty($match_id))
					{
						// code for swipe entry				   
						$match_id=insertdata('jp_users_company_match',$swipe_data);
						// code ends here
					}else{
							$swipe_cond="match_id = '$match_id'";
							$match_row_data = get_row_data('jp_users_company_match',$swipe_cond,'*');
							
							$job_post_id = $match_row_data['job_id'];
							$user_status = $match_row_data['user_status'];
							
							update_table('jp_users_company_match',$swipe_cond,$swipe_data);
					}					
				// code ends here for swipe
				
				
				$response_message =$this->lang->line('companyswipe_failure_message');
				// code for sending notification to the user
				if($company_status=='A')
				{
					$response_message =$this->lang->line('companyswipe_success_message');
					
					$user_device_token = getArrayValue('jp_users','device_token,device_name','user_id',$user_id);
					
					//$job_title = getSingleValue('jp_job_posts','job_title','job_post_id',$job_post_id);
					
					$this->lang->load('pushtext');
					$company_accept_swap_job = $this->lang->line('company_accept_swap_job');
					
					$company_name = $isexist['company_name'];
										
					$token = array(
						'COMPANY_NAME' =>$company_name
					);
					$pattern = '[%s]';
					foreach($token as $key=>$val){
						$varMap[sprintf($pattern,$key)] = $val;
					}
					$final_accept_job_message = strtr($company_accept_swap_job,$varMap);
					
					if($user_device_token[0]['device_token'])
					{
						$user_token = $user_device_token[0]['device_token'];
						$user_title = 'Company shown interest in your profile';
						
						$log_user_data=array(
							'from_id'=>$company_id,
							'to_id'=>$user_id,
							'notification_text'=>$final_accept_job_message,
							'notification_type'=>'swapjob_push_notification',
							'role_id'=>2,
							'to_role_id'=>3
						);
						
						$payload_data=array('match_id'=>$match_id,'user_id'=>$user_id,'company_id'=>$company_id,'job_id'=>$job_post_id,'user_status'=>$user_status,'company_status'=>$company_status,'notify_category'=>'company_swap_profile');
						
						if($user_device_token[0]['device_name']=='A')
						{
							$user_push_status = send_push($user_token,$user_title, $final_accept_job_message,$log_user_data,$payload_data);
						}
						
						if($user_device_token[0]['device_name']=='I')
						{
							$user_push_status = send_ios_push($user_token,$user_title, $final_accept_job_message,$log_user_data,$payload_data);
						}
					}					
				}
				// code for notification ends here
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$this->empty_object,
					'message' =>$response_message
				],REST_Controller::HTTP_OK);	
			
		}
	}
	// function ends here
	
	// function to get the contact us details
	public function company_contact_us_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
		$this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('message', 'Message', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				$role_id = $user_data->roleId;
				
				$cond="company_id = '$user_id'";
				$isexist=exists_indb('jp_companies',$cond,'company_first_name, company_last_name,company_name,company_app_language');
				
				// super admin data
				
				$admin_cond="id = '1'";
				$admin_exist=exists_indb('jp_admin',$admin_cond,'firstname, lastname, to_email');
				
				// admin data ends here
				
				$user_app_langauge = $isexist['company_app_language'];
				$this->lang->load('authtext', $user_app_langauge);	
					
				$email = $this->input->post('email');
				$phone_code = $this->input->post('phone_code');
				$mobile_no = $this->input->post('mobile_no');
				$message = $this->input->post('message');
				
				$name = $isexist['company_name'];
				
				$contact_data=array(
					'contact_name'=>$name,
					'contact_email'=>$email,
					'from_id'=>$user_id,
					'to_id'=>1,
					'phone_code'=>$phone_code,
					'mobile_no'=>$mobile_no,
					'message'=>$message,
					'from_role_id'=>$role_id,
					'to_role_id'=>1,
					'created_at'=>date('Y-m-d H:i:s')
				);
				
				$result = insertdata('jp_contact_us',$contact_data);
				if($result)
				{
					$contact_mobile=$phone_code.$mobile_no;
					$admin_name = $admin_exist['firstname'].' '.$admin_exist['lastname'];
					
					$mail_data['result']=array(
						"name" => $admin_name,
						"contact_name" =>$name,
						"contact_email" =>$email,
						"contact_mobile" =>$contact_mobile,
						"message"=>$message
					);
					
					//Send Email With OTP To User
					$to=$admin_exist['to_email'];
					$subject="Contact us details";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/contact_us',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					$log_data=array(
						'from_id'=>$user_id,
						'to_id'=>1,
						'notification_text'=>$message,
						'notification_type'=>'contact_us',
						'role_id'=>$role_id,
						'created_at'=>date('Y-m-d h:i:s')
					);
					
					insert_log_data('jp_notifications',$log_data);
					
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('register_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('resend_email_tan_not_sent')
						], REST_Controller::HTTP_OK);
				}				
		}
	}
	// function ends here
	
	// function to list the company notifications
	public function company_notifications_list_get()
	{
		$data=array();
		$user_token=$this->input->get('user_token');		
		$user_data = jwt_helper::decode($user_token);
		$company_id = $user_data->userId;
		
		$cond="company_id = '$company_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
		
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$seen_data=array('is_seen'=>1);
		$seen_cond = "to_id = '$company_id' AND to_role_id=2";
		$result = update_table('jp_notifications',$seen_cond,$seen_data);
		
		$notification_data = $this->companies_model->get_notifications($company_id);
		//		print_r($notification_data);
		if(!empty($notification_data))
		{
			foreach($notification_data as $notify)
			{
				if($notify['role_id']=='3')
				{
					$user_id = $notify['from_id'];
					$company_cond="user_id = '$user_id'";
					$company_isexist=exists_indb('jp_users',$company_cond,'first_name,last_name');
					
					$time = strtotime($notify['created_at']);
					$created_at = human_timing($time);
					
					$data[]=array(
					'first_name'=>$company_isexist['first_name'],
					'last_name'=>$company_isexist['last_name'],
					'notification_text'=>$notify['notification_text'],
					'created_at'=>$created_at
					);						
				}	
				
				/*if($notify['role_id']=='1')
				{
					$user_id = $notify['from_id'];
					$admin_cond="id = $user_id";
					$admin_exist=exists_indb('jp_admin',$admin_cond,'firstname, lastname, to_email');
					
					//$company_cond="user_id = '$user_id'";
					//$company_isexist=exists_indb('jp_users',$company_cond,'first_name,last_name');
					
					$time = strtotime($notify['created_at']);
					$created_at = human_timing($time);
					
					$data[]=array(
					'first_name'=>$admin_exist['first_name'],
					'last_name'=>$admin_exist['last_name'],
					'notification_text'=>$notify['notification_text'],
					'created_at'=>$created_at
					);						
				}*/
				
			}			
				$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('data_found')
				],REST_Controller::HTTP_OK);
			
						
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$data,
					'message' =>$this->lang->line('user_notification_no_data_found')
				], REST_Controller::HTTP_OK);
		}
		
	}
	// function ends here
	
	// function for company accepted users
	// function to list the users
	public function company_accepted_users_listing_get()
	{
		$users_listing = array();
		$users_data='';
		
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$company_accepted_users =array();
		//$swipe_users = $this->companies_model->swipe_accepted_users($user_id);
		$swipe_condition ="company_id = '$user_id' AND user_status='NA' AND company_status='A'";
		$swipe_users = get_all_data('jp_users_company_match',$swipe_condition,'*');
		if(!empty($swipe_users))
		{
			foreach($swipe_users as $swp_user)
			{
				if(($swp_user['user_status']=='NA') && ($swp_user['company_status']=='A'))
				{
					$company_accepted_users[]=$swp_user['user_id'];					
				}
			}
		}
		
		// code ends here for industry 
		if(!empty($company_accepted_users))
		{			
			$users_data = $this->companies_model->get_accepted_users($company_accepted_users);
		}
		
		if(!empty($users_data))
		{
			foreach($users_data as $users)
			{
				/*if(!in_array($users['user_id'], $company_rejected_users))
				{*/
					$match_id= '';
					$company_match_status='';
					$user_match_status='';
					$job_id='';
				
					if(!empty($swipe_users))
					{
						foreach($swipe_users as $swp_user)
						{
							if($users['user_id']==$swp_user['user_id'])
							{
								$match_id = $swp_user['match_id'];
								$company_match_status = $swp_user['company_status'];
								$user_match_status = $swp_user['user_status'];
								$job_id = $swp_user['job_id'];
							}
						}
					}
					
					$user_profile = base_url().'public/dist/img/avatar5.png';
					if(!empty($users['profile_image']))
					{
						$user_profile = base_url().'public/images/user_docs/'.$users['profile_image'];
					}
					
					$country_name='';
					if(!empty($users['country']))
					{
						$country_name = getSingleValue('jp_countries','name','id',$users['country']);
					}
					
					$state_name='';
					if(!empty($users['state']))
					{
						$state_name = getSingleValue('jp_states','name','id',$users['state']);
					}
					
					$city_name='';
					if(!empty($users['city']))
					{
						$city_name = getSingleValue('jp_cities','name','id',$users['city']);
					}
					
					$gender_name='';
					if(!empty($users['gender_id']))
					{
						$gender_name = getSingleValue('jp_genders','gender_name','gender_id',$users['gender_id']);
					}
					
					$user_dob='';
					if(!empty($users['dob']))
					{
						$user_dob = $users['dob'];
					}
					
					$user_age='';
					if(!empty($users['dob']))
					{
						$from = new DateTime($users['dob']);
						$to = new DateTime('today');
						$user_age = $from->diff($to)->y;
					}
					
					$work_experience='';
					if(!empty($users['experience_id']))
					{
						$work_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$users['experience_id']);
					}
					
					$carrier_objective='';
					if(!empty($users['profile_summary']))
					{
						$carrier_objective = $users['profile_summary'];
					}
					
					$skills_data = $this->users_model->users_skills($users['user_id']);
					$user_key_skills=array();
					if(!empty($skills_data))
					{
						foreach($skills_data as $skills)
						{
							$skill_name='';
							if(!empty($skills['skill_id']))
							{
								$skill_name = getSingleValue('jp_job_skills','job_skill_name','job_skill_id',$skills['skill_id']);
							}
							$user_key_skills[]=$skill_name;
						}
					}
					
					$education_data = $this->users_model->education_data($users['user_id']);
					$degree_name='';
					if(!empty($education_data))
					{
						foreach($education_data as $education)
						{						
							if(!empty($education['degree_type_id']))
							{
								$degree_name = getSingleValue('jp_degree_type','job_degree_type_name','job_degree_type_id',$education['degree_type_id']);
							}						
						}
					}
					
					$users_listing[] = array
					(
						'user_id' => $users['user_id'],
						'first_name'=>$users['first_name'],
						'middle_name'=>$users['middle_name'],
						'last_name'=>$users['last_name'],
						'user_profile'=>$user_profile,
						'email'=>$users['email'],
						'degree_name'=>$degree_name,
						'skill_name'=>$user_key_skills,
						'mobile_no' =>$users['mobile_no'],
						'phone_code'=>$users['phone_code'],
						'country'=>$country_name,
						'state'=>$state_name,
						'city'=>$city_name,
						'user_age'=>$user_age,
						'user_dob'=>$user_dob,
						'gender'=>$gender_name,
						'match_id'=>$match_id,
						'company_match_status'=>$company_match_status,
						'user_match_status'=>$user_match_status,
						'job_id'=>$job_id
					);
				/*}*/
			}
			
			$data=array(
				'users_listing'=>$users_listing
			);
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('companyhomeusers_no_data_found')
				], REST_Controller::HTTP_OK);
		}
	}
	// function ends here
	
	// function to list the users
	public function common_accepted_users_listing_get()
	{
		$advert_company_radius = $this->config->item('advert_company_radius');
		$advert_companies =array();
		
		$users_listing = array();
		$users_data='';
		
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="company_id = '$user_id'";
		$isexist=exists_indb('jp_companies',$cond,'company_latitude,company_longitude,company_app_language');
				
		$user_app_langauge = $isexist['company_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		if((!empty($isexist['company_latitude']+0)) && (!empty($isexist['company_longitude']+0)))
		{
			$radius_companies = radar_company_search($user_id,$advert_company_radius,$isexist['company_latitude'],$isexist['company_longitude']);		
			if(!empty($radius_companies))
			{
				$valid_company_ids = get_active_company_ads();
				if(!empty($valid_company_ids))
				{
					foreach($valid_company_ids as $ad_comp)
					{
						if(in_array($ad_comp, $radius_companies))
						{
							$advert_cond ="company_id = '$ad_comp'";
							$advert_data = get_row_data('jp_companies',$advert_cond,'company_id,company_name, company_logo,company_website');
							
							$ad_company_logo = base_url().'public/images/company_app_no_logo.png';
							if(!empty($advert_data['company_logo']))
							{
								$ad_company_logo = base_url().'public/images/company_docs/'.$advert_data['company_logo'];					
								
							}
							
							
							$advert_companies[]=array
							(
								'company_id'=>$advert_data['company_id'],
								'company_name'=>$advert_data['company_name'],
								'company_logo'=>$ad_company_logo,
								'company_website'=>$advert_data['company_website']
							);
							
						}	
					}
				}				
			}	
		}				
		// company adds alogorithms ens here
		
		$company_accepted_users =array();
		//$swipe_users = $this->companies_model->swipe_accepted_users($user_id);
		$swipe_condition ="company_id = '$user_id' AND user_status='A' AND company_status='A'";
		$swipe_users = get_all_data('jp_users_company_match',$swipe_condition,'*');
		if(!empty($swipe_users))
		{
			foreach($swipe_users as $swp_user)
			{
				$company_accepted_users[]=$swp_user['user_id'];	
			}
		}
		
		// code ends here for industry 
		if(!empty($company_accepted_users))
		{			
			$users_data = $this->companies_model->get_accepted_users($company_accepted_users);
		}
		
		if(!empty($users_data))
		{
			foreach($users_data as $users)
			{
				/*if(!in_array($users['user_id'], $company_rejected_users))
				{*/
					$match_id= '';
					$company_match_status='';
					$user_match_status='';
					$job_id='';
				
					if(!empty($swipe_users))
					{
						foreach($swipe_users as $swp_user)
						{
							if($users['user_id']==$swp_user['user_id'])
							{
								$match_id = $swp_user['match_id'];
								$company_match_status = $swp_user['company_status'];
								$user_match_status = $swp_user['user_status'];
								$job_id = $swp_user['job_id'];
							}
						}
					}
					
					$user_profile = base_url().'public/dist/img/avatar5.png';
					if(!empty($users['profile_image']))
					{
						$user_profile = base_url().'public/images/user_docs/'.$users['profile_image'];
					}
					
					$country_name='';
					if(!empty($users['country']))
					{
						$country_name = getSingleValue('jp_countries','name','id',$users['country']);
					}
					
					$state_name='';
					if(!empty($users['state']))
					{
						$state_name = getSingleValue('jp_states','name','id',$users['state']);
					}
					
					$city_name='';
					if(!empty($users['city']))
					{
						$city_name = getSingleValue('jp_cities','name','id',$users['city']);
					}
					
					$gender_name='';
					if(!empty($users['gender_id']))
					{
						$gender_name = getSingleValue('jp_genders','gender_name','gender_id',$users['gender_id']);
					}
					
					$user_dob='';
					if(!empty($users['dob']))
					{
						$user_dob = $users['dob'];
					}
					
					$user_age='';
					if(!empty($users['dob']))
					{
						$from = new DateTime($users['dob']);
						$to = new DateTime('today');
						$user_age = $from->diff($to)->y;
					}
					
					$work_experience='';
					if(!empty($users['experience_id']))
					{
						$work_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$users['experience_id']);
					}
					
					$carrier_objective='';
					if(!empty($users['profile_summary']))
					{
						$carrier_objective = $users['profile_summary'];
					}
					
					$skills_data = $this->users_model->users_skills($users['user_id']);
					$user_key_skills=array();
					if(!empty($skills_data))
					{
						foreach($skills_data as $skills)
						{
							$skill_name='';
							if(!empty($skills['skill_id']))
							{
								$skill_name = getSingleValue('jp_job_skills','job_skill_name','job_skill_id',$skills['skill_id']);
							}
							$user_key_skills[]=$skill_name;
						}
					}
					
					$education_data = $this->users_model->education_data($users['user_id']);
					$degree_name='';
					if(!empty($education_data))
					{
						foreach($education_data as $education)
						{						
							if(!empty($education['degree_type_id']))
							{
								$degree_name = getSingleValue('jp_degree_type','job_degree_type_name','job_degree_type_id',$education['degree_type_id']);
							}						
						}
					}
					
					$users_listing[] = array
					(
						'user_id' => $users['user_id'],
						'first_name'=>$users['first_name'],
						'middle_name'=>$users['middle_name'],
						'last_name'=>$users['last_name'],
						'user_profile'=>$user_profile,
						'email'=>$users['email'],
						'degree_name'=>$degree_name,
						'skill_name'=>$user_key_skills,
						'mobile_no' =>$users['mobile_no'],
						'phone_code'=>$users['phone_code'],
						'country'=>$country_name,
						'state'=>$state_name,
						'city'=>$city_name,
						'user_age'=>$user_age,
						'user_dob'=>$user_dob,
						'gender'=>$gender_name,
						'match_id'=>$match_id,
						'company_match_status'=>$company_match_status,
						'user_match_status'=>$user_match_status,
						'job_id'=>$job_id
					);
				/*}*/
			}
			
			$data=array(
				'advert_companies'=>$advert_companies,
				'users_listing'=>$users_listing
			);
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('companyhomeusers_no_data_found')
				], REST_Controller::HTTP_OK);
		}
	}
	// function ends here
	
	function convert_number_to_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			100000             => 'lakh',
			10000000          => 'crore'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
			);
			return false;
		}

		if ($number < 0) {
			return $negative . $this->convert_number_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}

		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . $this->convert_number_to_words($remainder);
				}
				break;
			case $number < 100000:
				$thousands   = ((int) ($number / 1000));
				$remainder = $number % 1000;

				$thousands = $this->convert_number_to_words($thousands);

				$string .= $thousands . ' ' . $dictionary[1000];
				if ($remainder) {
					$string .= $separator . $this->convert_number_to_words($remainder);
				}
				break;
			case $number < 10000000:
				$lakhs   = ((int) ($number / 100000));
				$remainder = $number % 100000;

				$lakhs = $this->convert_number_to_words($lakhs);

				$string = $lakhs . ' ' . $dictionary[100000];
				if ($remainder) {
					$string .= $separator . $this->convert_number_to_words($remainder);
				}
				break;
			case $number < 1000000000:
				$crores   = ((int) ($number / 10000000));
				$remainder = $number % 10000000;

				$crores = $this->convert_number_to_words($crores);

				$string = $crores . ' ' . $dictionary[10000000];
				if ($remainder) {
					$string .= $separator . $this->convert_number_to_words($remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= $this->convert_number_to_words($remainder);
				}
				break;
		}

		/*if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}*/

		return $string;
	}
}