<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');

// Use the REST API Client to make requests to the Twilio REST API
//use Twilio\Rest\Client;

class Users extends REST_Controller
{
	public $empty_object;
	function __construct(){
		parent::__construct();
		$this->load->helper('jwt_helper');
		$this->load->model('app_api/users_model', 'users_model');
		$this->load->helper('email_helper');
		//$this->load->helper('string');
		$this->lang->load('authtext');
		$this->load->helper('fcmpush_helper');
		$this->empty_object = json_decode('{}');
		
		ini_set('post_max_size','20M');
		ini_set('upload_max_size','10M');
		ini_set('memory_limit','128M');
		ini_set('max_execution_time','5000');
	}
	
	// function to send the email OTP
	public function verify_user_email_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		//$this->form_validation->set_rules('email', 'Email', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
				'status' => false,
				'code' => 400,
				'data'=>$this->empty_object,
				'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
				//$user_email=$this->input->post('email');
			
				$user_data = jwt_helper::decode($user_token);
				
				$user_id = $user_data->userId;
				
				$checker_data=array
				(
					'user_id'=>$user_id
				);
				
				$user_checker = $this->users_model->check_user($checker_data);
				
				if($user_checker)
				{
					$user_name = $user_checker['first_name'].' '.$user_checker['last_name'];
					$email_otp=mt_rand(1000,9999);
					$mail_data['result']=array(
						"name" => $user_name,
						"email_tan" => $email_otp,
					);
					
					//Send Email With OTP To User
					$to=$user_checker['email'];
					$subject="Email OTP";
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/email_tan',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					// Update User Email Tan
					$user_otp_update=$this->users_model->user_otp_update($user_id, $email_otp);
					if($emailsent && $user_otp_update)
					{
						$data=array(
							'email_otp'=>$email_otp
						);
				
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$data,
							'message' =>'Please check mail for OTP'
						],REST_Controller::HTTP_OK);
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>'Email not sent, Try again later'
							], REST_Controller::HTTP_OK);
					}
					
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>'User not exist for this email'
						], REST_Controller::HTTP_OK);
				}				
		}	
	}
	// function for email tan ends here
	
	// function to varify the email tan
	public function email_verify_otp_post()
	{
		$this->form_validation->set_rules('email_otp', 'Email OTP', 'trim|required');
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$email_otp=$this->input->post('email_otp');
				$user_token=$this->input->post('user_token');
			
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$otp_data=array(
					'user_id'=>$user_id,
					'email_otp'=>$email_otp
				);
				
				$email_otp_verify = $this->users_model->email_otp_verify($otp_data);
				
				if($email_otp_verify)
				{
					$cond="user_id = '".$user_id."'";
					$isexist=exists_indb('jp_users',$cond,'mobile_no,phone_code,is_mobile_verify,user_app_language');
					
					$user_app_langauge = $isexist['user_app_language'];
					$this->lang->load('authtext', $user_app_langauge);
					
					$mobile_otp='';
					if($isexist['is_mobile_verify']=='N')
					{
						$this->lang->load('smstext',$user_app_langauge);
						$mobile_otp=mt_rand(1000,9999);
						
						$data_email=array('mobile_otp' => $mobile_otp);
						$new_condition="user_id='".$user_id."'";
						$update_mobile=update_table('jp_users',$new_condition,$data_email);
						
						$full_mobile = $isexist['phone_code'].$isexist['mobile_no'];
						$sms_text = $this->lang->line('sms_otp');					
															
						$final_sms_text = sprintf($sms_text, $mobile_otp);
						
						$this->load->helper('sms_helper');
						$sms_status = send_sms($full_mobile, $final_sms_text);
					}
					
					$data=array(
						'mobile_otp'=>$mobile_otp
					);
					
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('email_success')
					],REST_Controller::HTTP_OK);
				}else{
						$this->response([
							'status' => false,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('email_verify_wrong_tan')
						], REST_Controller::HTTP_OK);
				}
		}	
	}
	// function ends here
	
	// function to edit the user email	
	public function user_edit_email_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[jp_users.email]|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
		],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
			
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$email  = $this->input->post('email');
				
				$cond="user_id = '".$user_id."'";
				$isexist=exists_indb('jp_users',$cond,'first_name,last_name,user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$email_otp=mt_rand(1000,9999);
				$user_name = $isexist['first_name'].' '.$isexist['last_name'];
				
				$mail_data['result']=array(
					"name" => $user_name,
					"email_tan" => $email_otp,
				);
						
				$data_email=array('email' => $email, 'email_otp' => $email_otp);
				$new_condition="user_id='".$user_id."'";
				$update_mobile=update_table('jp_users',$new_condition,$data_email);
				
				//Send Email With OTP To User
				$to=$email;
				$subject="Email OTP";
				$attachment='';
				$cc='';
				$body=$this->load->view('email_template/email_tan',$mail_data,true);
				$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
								
				$data=array(
				'email_otp'=>$email_otp
				);

				if($emailsent)
				{
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('resend_email_tan_successfully')
					],REST_Controller::HTTP_OK);
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('resend_email_tan_not_sent')
						], REST_Controller::HTTP_OK);
				}
		}	
	}
	// function ends here for email edit
	
	// function to edit the mobile number
	public function user_edit_mobile_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
				'status' => false,
				'code' => 400,
				'data'=>$this->empty_object,
				'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
				$mobile_no=$this->input->post('mobile_no');
				$phone_code=$this->input->post('phone_code');
				
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$user_app_language = $this->input->post('user_app_language');			
				$this->lang->load('authtext', $user_app_language);
				
				$this->lang->load('smstext',$user_app_language);
				
				$cond="user_id = '".$user_id."'";
				$isexist=exists_indb('jp_users',$cond,'first_name,last_name,user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$this->lang->load('smstext',$user_app_langauge);
				
				$mobile_otp=mt_rand(1000,9999);
				
				$data_email=array('mobile_no'=>$mobile_no, 'phone_code'=>$phone_code ,'mobile_otp' => $mobile_otp);
				$new_condition="user_id='".$user_id."'";
				$update_mobile=update_table('jp_users',$new_condition,$data_email);
				
				$full_mobile = $phone_code.$mobile_no;
				$sms_text = $this->lang->line('sms_otp');					
													
				$final_sms_text = sprintf($sms_text, $mobile_otp);
				
				$this->load->helper('sms_helper');
				$sms_status = send_sms($full_mobile, $final_sms_text);
				
				$handle_status = explode('*#',$sms_status);
				
				if($handle_status[0])
				{
					$data=array(
						'mobile_otp'=>$mobile_otp
					);
			
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('resend_mobile_sent')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$this->empty_object,
							'message' =>$handle_status[1]
						], REST_Controller::HTTP_OK);
				}
		}		
	}
	// function ends here for mobile number
	
	// function to send the mobile verify otp
	public function verify_user_mobile_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		//$this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required');
		//$this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
				'status' => false,
				'code' => 400,
				'data'=>$this->empty_object,
				'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');
				//$mobile_no=$this->input->post('mobile_no');
				//$phone_code=$this->input->post('phone_code');
			
				$user_data = jwt_helper::decode($user_token);
				
				$user_id = $user_data->userId;
				
				$checker_data=array
				(
					'user_id'=>$user_id
				);
				
				$user_checker = $this->users_model->check_user_mobile($checker_data);
				
				if($user_checker)
				{
					$this->lang->load('smstext');
					
					$mobile_no=$user_checker['mobile_no'];
					$phone_code=$user_checker['phone_code'];
					
					$full_mobile = $phone_code.$mobile_no;
					$sms_text = $this->lang->line('sms_otp');					
					$mobile_otp=mt_rand(1000,9999);
										
					$final_sms_text = sprintf($sms_text, $mobile_otp);
					
					$this->load->helper('sms_helper');
					$sms_status = send_sms($full_mobile, $final_sms_text);
					
					$handle_status = explode('*#',$sms_status);
					
					// Update User mobile OTP
					$usersms_otp_update=$this->users_model->usersms_otp_update($user_id, $mobile_otp);
					
					if($handle_status[0])
					{
						$data=array(
							'mobile_otp'=>$mobile_otp
						);
				
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$data,
							'message' =>'Please check mobile for OTP'
						],REST_Controller::HTTP_OK);
						
					}else{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$handle_status[1]
							], REST_Controller::HTTP_OK);
					}										
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>'User not exist with this mobile number'
						], REST_Controller::HTTP_OK);
				}				
		}	
	}
	// function for email tan ends here
	
	// function to varify the email tan
	public function mobile_verify_otp_post()
	{
		$this->form_validation->set_rules('mobile_otp', 'Mobile OTP', 'trim|required');
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))		 
			],REST_Controller::HTTP_OK);
		}else{
				$mobile_otp=$this->input->post('mobile_otp');
				$user_token=$this->input->post('user_token');
			
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$user_app_language = $this->input->post('user_app_language');			
				$this->lang->load('authtext', $user_app_language);
				
				$otp_data=array(
					'user_id'=>$user_id,
					'mobile_otp'=>$mobile_otp
				);
				
				$mobile_verify = $this->users_model->mobile_otp_verify($otp_data);
				
				if($mobile_verify)
				{
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('mobile_verify_success')
					],REST_Controller::HTTP_OK);
				}else{
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('mobile_tan_is_wrong')
						], REST_Controller::HTTP_OK);
				}
		}	
	}
	// function ends here
	
	// function to send reset password link
	public function user_forgot_password_post()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message'=>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))],REST_Controller::HTTP_OK);	
		}else
		{
				$email = $this->input->post('email');
				
				$user_app_language = $this->input->post('user_app_language');			
				$this->lang->load('authtext', $user_app_language);
				
				$cond="email = '$email'";
				$isexist=exists_indb('jp_users',$cond,'email,first_name,last_name,user_app_language');
				
				if($isexist)
				{
					$user_name = $isexist['first_name'].' '.$isexist['last_name'];
					$token=RandomStringGenerator(30);
					
					$user_app_langauge = $isexist['user_app_language'];
					$this->lang->load('authtext', $user_app_langauge);
					
					$mail_data['result']=array
					(
						"token" =>$token,
						"name" =>$user_name
					);
					
					$update_data=array
					(
						'forgot_password_token'=>$token,
						'p_token_expires_at' => date('Y-m-d H:i:s', strtotime('24 hours'))
					);
								
					$update_token=update_table('jp_users',$cond,$update_data);
					
					if($update_token)
					{
						$to=$email;
						$subject=$this->lang->line('forgot_email_subject');
						$attachment='';
						$cc='';
						$body=$this->load->view('email_template/reset_password_link',$mail_data,true);
						$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
						
						$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('forgot_email_sent')
							],REST_Controller::HTTP_OK);
					}else{
						$this->response([
						'status'=> false,
						'code' => 401,
						'data'=>$this->empty_object,
						'message'=>$this->lang->line('forgot_email_not_sent')
						],REST_Controller::HTTP_OK);
					}
					
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('forgot_email_not_found')
						], REST_Controller::HTTP_OK);
				}
		}
	}
	// function ends here
	
	// function to fetch the user profile
	public function user_account_details_get()
	{
		$user_profile_data = array();
		$user_carrier_objective=array();
		$user_key_skills=array();
		$user_job_types=array();
		$user_work_experience=array();
		$user_eductaion=array();
		$user_languages=array();
		$user_resumes=array();
		$user_preferences=array();
		$user_videos=array();
		
		$user_token=$this->input->get('user_token');
		
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="user_id = '$user_id'";
		$isexist=exists_indb('jp_users',$cond,'*');
		
		if($isexist)
		{
			$user_app_langauge = $isexist['user_app_language'];
			$this->lang->load('authtext', $user_app_langauge);
			
			$user_profile = base_url().'public/dist/img/avatar5.png';
			if(!empty($isexist['profile_image']))
			{
				$user_profile = base_url().'public/images/user_docs/'.$isexist['profile_image'];
			}
			
			$country_name='';
			if(!empty($isexist['country']))
			{
				$country_name = getSingleValue('jp_countries','name','id',$isexist['country']);
			}
			
			$state_name='';
			if(!empty($isexist['state']))
			{
				$state_name = getSingleValue('jp_states','name','id',$isexist['state']);
			}
			
			$city_name='';
			if(!empty($isexist['city']))
			{
				$city_name = getSingleValue('jp_cities','name','id',$isexist['city']);
			}
			
			$gender_name='';
			if(!empty($isexist['gender_id']))
			{
				$gender_name = getSingleValue('jp_genders','gender_name','gender_id',$isexist['gender_id']);
			}
			
			$user_dob='';
			if(!empty($isexist['dob']))
			{
				$user_dob = $isexist['dob'];
			}
			
			$user_age='';
			if(!empty($isexist['dob']))
			{
				$from = new DateTime($isexist['dob']);
				$to = new DateTime('today');
				$user_age = $from->diff($to)->y;
			}
			
			$work_experience='';
			if(!empty($isexist['experience_id']))
			{
				$work_experience = getSingleValue('jp_job_experiences','job_experience_name','job_experience_id',$isexist['experience_id']);
			}
			
			$carrier_objective='';
			if(!empty($isexist['profile_summary']))
			{
				$carrier_objective = $isexist['profile_summary'];
			}
			
			$skills_data = $this->users_model->users_skills($user_id);
			if(!empty($skills_data))
			{
				foreach($skills_data as $skills)
				{
					$skill_name='';
					if(!empty($skills['skill_id']))
					{
						$skill_name = getSingleValue('jp_job_skills','job_skill_name','job_skill_id',$skills['skill_id']);
					}
					$user_key_skills[]=array
					(
						'skill_id'=>$skills['skill_id'],
						'skill_name'=>$skill_name
					);
				}
			}
			
			$job_types_data = $this->users_model->users_job_types($user_id);
			if(!empty($job_types_data))
			{
				foreach($job_types_data as $skills)
				{
					$job_type_name='';
					if(!empty($skills['jobtype_id']))
					{
						$job_type_name = getSingleValue('jp_job_types','job_type_name','job_type_id',$skills['jobtype_id']);
					}
					$user_job_types[]=array
					(
						'job_type_id'=>$skills['jobtype_id'],
						'job_type_name'=>$job_type_name
					);
				}
			}		
			
			$work_experience_data = $this->users_model->work_experience($user_id);
			if(!empty($work_experience_data))
			{
				foreach($work_experience_data as $work)
				{
					$experience_title='';
					if(!empty($work['experience_title']))
					{
						$experience_title = getSingleValue('jp_functional_areas','functional_name','functional_id',$work['experience_title']);
					}
					
					$work_from_datefmt= date("F Y", strtotime($work['start_date']));
					$work_to_datefmt= date("F Y", strtotime($work['end_date']));
					
					$user_work_experience[]=array
					(	
						'user_experience_id'=>$work['user_experience_id'],
						'company_name'=>$work['company_name'],
						'experience_title'=>$experience_title,
						'functional_id'=>$work['experience_title'],
						'work_from'=>$work_from_datefmt,
						'work_to'=>$work_to_datefmt,
						'start_date'=>$work['start_date'],
						'end_date'=>$work['end_date'],
						'currently_working'=>$work['currently_working'],
						'description'=>$work['experience_description']
					);
				}
			}
			
			$education_data = $this->users_model->education_data($user_id);
			if(!empty($education_data))
			{
				foreach($education_data as $education)
				{
					$university_name='';
					if(!empty($education['college_name']))
					{
						$university_name = getSingleValue('jp_university_college','university_college_name','university_college_id',$education['college_name']);
					}
					
					$degree_level='';
					if(!empty($education['degree_level_id']))
					{
						$degree_level = getSingleValue('jp_degree_level','job_degree_level_name','job_degree_level_id',$education['degree_level_id']);
					}
					
					$degree_name='';
					if(!empty($education['degree_type_id']))
					{
						$degree_name = getSingleValue('jp_degree_type','job_degree_type_name','job_degree_type_id',$education['degree_type_id']);
					}
					
					$start_date='';
					if(!empty($education['start_date']))
					{
						$start_date = $education['start_date'];
					}
					
					$end_date='';
					if(!empty($education['end_date']))
					{
						$end_date = $education['end_date'];
					}
					
					$education_from_datefmt= date("F Y", strtotime($start_date));
					$education_to_datefmt= date("F Y", strtotime($end_date));
					
					$user_eductaion[]=array
					(
						'user_education_id'=>$education['user_education_id'],
						'college_university_id'=>$education['college_name'],
						'college_university_name'=>$university_name,
						'degree_level_id'=>$education['degree_level_id'],
						'degree_level'=>$degree_level,
						'degree_type_id'=>$education['degree_type_id'],
						'degree_name'=>$degree_name,
						'from'=>$education_from_datefmt,
						'to'=>$education_to_datefmt,
						'start_date'=>$start_date,
						'end_date'=>$end_date,
						'currently_pursuing'=>$education['currently_pursuing']
					);
				}
			}
			
			$prefrences_data = $this->users_model->prefrences_data($user_id);
			if(!empty($prefrences_data))
			{
				foreach($prefrences_data as $preference)
				{
					$location_ids=explode(',',$preference['location_ids']);
					
					$preference_locations=array();
					if(!empty($location_ids))
					{
						foreach($location_ids as $loc_id)
						{
							$location_name = getSingleValue('jp_cities','name','id',$loc_id);
							$preference_locations[]=array
							(
								'location_id'=>$loc_id,
								'location_name'=>$location_name
							);
						}
					}
					
					$industry_ids=explode(',',$preference['industry_ids']);
					
					$preference_industry=array();
					if(!empty($industry_ids))
					{
						foreach($industry_ids as $indus_id)
						{
							$industry_name = getSingleValue('jp_industries','industry_name','industry_id',$indus_id);
							$preference_industry[]=array
							(
								'industry_id'=>$indus_id,
								'industry_name'=>$industry_name
							);
						}
					}
					
					$expected_salary_number = '';
					$expected_salary_words ='';
					$salary_array =array();
					if(!empty($preference['expected_salary']))
					{
						$expected_salary_words = $this->convert_number_to_words($preference['expected_salary']+0);
						$salary_array=array
						(
							'expected_salary_number'=>$preference['expected_salary']+0,
							'expected_salary_words'=>$expected_salary_words
						);						
					}
					
					$user_preferences[]=array
					(
						'preference_id'=>$preference['preference_id'],
						'location_data'=>$preference_locations,
						'industry_data'=>$preference_industry,
						'expected_salary'=>$salary_array
					);
				}
			}
			
			$language_data = $this->users_model->language_data($user_id);
			if(!empty($language_data))
			{
				foreach($language_data as $language)
				{
					$language_name='';
					if(!empty($language['language_id']))
					{
						$language_name = getSingleValue('jp_languages','language_name','language_id',$language['language_id']);
					}
															
					$user_languages[]=array
					(
						'language_id'=>$language['language_id'],
						'language_name'=>$language_name
					);
				}
			}
			
			$resumes_data = $this->users_model->resumes_data($user_id);
			if(!empty($resumes_data))
			{
				foreach($resumes_data as $resumes)
				{
					$user_resumes=array
					(
						'cv_id'=>$resumes['user_cv_id'],
						'cv_title'=>$resumes['user_cv_title'],
						'cv_file'=>$resumes['orignal_cv_name'],
						'cv_file_link'=>base_url().'public/images/user_docs/'.$resumes['user_cv_name']
					);
				}
			}else{
					$user_resumes = $this->empty_object;
			}
			
			$video_data = $this->users_model->video_data($user_id);
			if(!empty($video_data))
			{
				foreach($video_data as $video)
				{
					$user_videos=array
					(
						'video_id'=>$video['video_id'],
						'video_title'=>$video['video_title'],
						'video_file'=>$video['orignal_video_name'],
						'video_file_link'=>base_url().'public/images/user_docs/'.$video['video_name']
					);
				}
			}else{
					$user_videos = $this->empty_object;
			}
			
			$advert_company_radius = $this->config->item('advert_company_radius');
			
			// code for notification count
			$notification_counter='';
			$notification_count_data = $this->users_model->get_notifications_count($user_id);
			if(!empty($notification_count_data))
			{
				$notification_counter=$notification_count_data;
											
			}	
			// code for notification count ends here
			
			$user_profile_data = array
			(
				'user_id' => $user_id,
				'first_name'=>$isexist['first_name'],
				'middle_name'=>$isexist['middle_name'],
				'last_name'=>$isexist['last_name'],
				'user_profile'=>$user_profile,
				'email'=>$isexist['email'],
				'mobile_no' =>$isexist['mobile_no'],
				'phone_code'=>$isexist['phone_code'],
				'country'=>$country_name,
				'state'=>$state_name,
				'city'=>$city_name,
				'latitude'=>$isexist['latitude'],
				'longitude'=>$isexist['longitude'],
				'default_radius'=>$advert_company_radius,
				'user_age'=>$user_age,
				'user_dob'=>$user_dob,
				'work_experience'=>$work_experience,
				'gender'=>$gender_name,
				'email_verify'=>$isexist['is_email_verify'],
				'mobile_verify'=>$isexist['is_mobile_verify'],
				'user_token' =>$user_token
			);		
			
			$data =array
			(
				'user_profile_data'=>$user_profile_data,
				'user_carrier_objective'=>$carrier_objective,
				'user_key_skills'=>$user_key_skills,
				'user_job_types'=>$user_job_types,
				'user_work_experience'=>$user_work_experience,
				'user_eductaion'=>$user_eductaion,
				'user_preferences'=>$user_preferences,
				'user_languages'=>$user_languages,
				'user_resumes'=>$user_resumes,
				'user_videos'=>$user_videos,
				'user_notification_counter'=>$notification_counter
			);	
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message'=>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);			
		}else{
			$this->response([
				'status' => true,
				'code' =>401,
				'data'=>$this->empty_object,
				'message' =>$this->lang->line('employer_search_no_data_found')
			], REST_Controller::HTTP_OK);
		}		
		
	}
	// function ends here
	
	public function update_user_profile_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		
		//$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[jp_users.email]|required');
				
		//$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
		
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		
		//$this->form_validation->set_rules('address', 'Address', 'trim|required');
		//$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
		//$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
		
		$this->form_validation->set_rules('gender_id', 'Gender', 'trim|required');
				
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$user_token=$this->input->post('user_token');		
			$user_data = jwt_helper::decode($user_token);
			$user_id = $user_data->userId;
			
			$cond="user_id = '$user_id'";
			$isexist=exists_indb('jp_users',$cond,'profile_image,user_app_language');
			
			$user_app_langauge = $isexist['user_app_language'];
			$this->lang->load('authtext', $user_app_langauge);
			
			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			$middle_name=$this->input->post('middle_name');
			
			//$email=$this->input->post('email');
							
			//$password=$this->input->post('password');
						
			$country=$this->input->post('country');
			$state=$this->input->post('state');			
			$city=$this->input->post('city');
			
			//$autocomplete=$this->input->post('address');
			//$postal_code=$this->input->post('postal_code');
			
			//$latitude = $this->input->post('latitude');
			//$longitude = $this->input->post('longitude');
			
			//$mobile=$this->input->post('mobile_no');			
			//$phone_code=$this->input->post('phone_code');
			
			$gender_id=$this->input->post('gender_id');
			$dob = $this->input->post('dob');
						
            $user_data = array
				(
					'first_name'=>$first_name,
					'middle_name'=>$middle_name,
					'last_name'=>$last_name,
					'country'=>$country,
					'state'=>$state,
					'city'=>$city,
					'gender_id'=>$gender_id,
					'dob'=>$dob,
					"updated_at" => date('Y-m-d H:i:s')
				);
				
				$data = $this->security->xss_clean($user_data);				
				$result = update_table('jp_users',$cond,$data);
				if($result)
				{
					$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('update_success')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
						'status' => true,
						'code' => 401,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_fail')
						], REST_Controller::HTTP_OK);
				}			
		}
	}
	
	// function to update the user profile image
	public function update_user_imageprofile_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	
			],REST_Controller::HTTP_OK);
		}else{
			
			$user_token=$this->input->post('user_token');		
			$user_data = jwt_helper::decode($user_token);
			$user_id = $user_data->userId;
			
			$cond="user_id = '$user_id'";
			$isexist=exists_indb('jp_users',$cond,'profile_image,user_app_language');
			
			$user_app_langauge = $isexist['user_app_language'];
			$this->lang->load('authtext', $user_app_langauge);
									
			if(!empty($_FILES['profile_image']['size']))
			{
				$config['upload_path'] = './public/images/user_docs';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = 5000;
				$config['max_width'] = 5000;
				$config['max_height'] = 5000;
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config);
				
				if(!$this->upload->do_upload('profile_image'))
				{
					$error = $this->upload->display_errors();
										
					$this->response([
						'status' => false,
						'code' => 400,
						'data'=>$this->empty_object,
						'message' =>$error	
					],REST_Controller::HTTP_OK);
					
				}else{
					$profile_image = $this->upload->data('file_name');
				}
			}else{
					$profile_image = $isexist['profile_image'];
			}
			
            $user_data = array
				(
					'profile_image'=>$profile_image,
					"updated_at" => date('Y-m-d H:i:s')
				);
				
				$data = $this->security->xss_clean($user_data);				
				$result = update_table('jp_users',$cond,$data);
				if($result)
				{
					$image_path = base_url().'public/images/user_docs/'.$profile_image;
					
					$image_data=array('profile_image'=>$image_path);
					
					$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$image_data,
					'message' =>$this->lang->line('update_success')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
						'status' => true,
						'code' => 401,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_fail')
						], REST_Controller::HTTP_OK);
				}			
		}
	}
	// function ends here for user profile image
	
	// function to add or update the user carrier objective
	public function user_add_update_objective_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('carrier_objective', 'Carrier Objective', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$profile_summary = $this->input->post('carrier_objective');
			   
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'profile_image,user_app_language');
			
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$user_data=array(
					'profile_summary'=>$profile_summary
				);
				
				$data = $this->security->xss_clean($user_data);				
				$result = update_table('jp_users',$cond,$data);
				if($result)
				{
					$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('update_success')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
						'status' => true,
						'code' => 401,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_fail')
						], REST_Controller::HTTP_OK);
				}
		}
	}
	// function ends here
	public function user_add_update_skills_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('skill_ids', 'Skills', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_db_skills =array();
				
				$string_skill_ids = $this->input->post('skill_ids');
				$skill_ids = json_decode($string_skill_ids);
				
				//print_r($skill_ids);
				//die();
			   
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);				
				
				$user_saved_skills = get_all_data('jp_users_skills',$cond,'*');
				
				if(empty($user_saved_skills))
				{
					foreach($skill_ids as $skills)
					{
						$batch_skills[]=array
						(
							'user_id'=>$user_id,
							'skill_id'=>$skills,
							'created_at'=>date('Y-m-d H:i:s')
						);						 
					}
					
					$this->db->insert_batch('jp_users_skills', $batch_skills);
						
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('register_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
						foreach($user_saved_skills as $db_skills)
						{
							if(!in_array($db_skills['skill_id'], $skill_ids))
							{
								row_delete('jp_users_skills','user_skill_id',$db_skills['user_skill_id']);								
							}else{
									$user_db_skills[]=$db_skills['skill_id'];
							}
						}
						
						foreach($skill_ids as $app_skills)
						{
							if(!in_array($app_skills, $user_db_skills))
							{
								$single_skills=array
								(
									'user_id'=>$user_id,
									'skill_id'=>$app_skills,
									'created_at'=>date('Y-m-d H:i:s')
								);
								insertdata('jp_users_skills',$single_skills);
							}
						}
						
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_success')
					],REST_Controller::HTTP_OK);
				}
		}
	}
	
	// function to add and update the user job type
	public function user_add_update_jobtype_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('job_type_id', 'Job Types', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_db_skills =array();
				
				$string_job_type_ids = $this->input->post('job_type_id');
				$skill_ids = json_decode($string_job_type_ids);
				
				//print_r($skill_ids);
				//die();
			   
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);				
				
				$user_saved_skills = get_all_data('jp_users_jobtypes',$cond,'*');
				
				if(empty($user_saved_skills))
				{
					foreach($skill_ids as $skills)
					{
						$batch_skills[]=array
						(
							'user_id'=>$user_id,
							'jobtype_id'=>$skills,
							'created_at'=>date('Y-m-d H:i:s')
						);						 
					}
					
					$this->db->insert_batch('jp_users_jobtypes', $batch_skills);
						
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('register_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
						foreach($user_saved_skills as $db_skills)
						{
							if(!in_array($db_skills['jobtype_id'], $skill_ids))
							{
								row_delete('jp_users_jobtypes','user_jobtype_id',$db_skills['user_jobtype_id']);								
							}else{
									$user_db_skills[]=$db_skills['jobtype_id'];
							}
						}
						
						foreach($skill_ids as $app_skills)
						{
							if(!in_array($app_skills, $user_db_skills))
							{
								$single_skills=array
								(
									'user_id'=>$user_id,
									'jobtype_id'=>$app_skills,
									'created_at'=>date('Y-m-d H:i:s')
								);
								insertdata('jp_users_jobtypes',$single_skills);
							}
						}
						
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_success')
					],REST_Controller::HTTP_OK);
				}
		}
	}
	// function ends here
	
	// function to add or update the user experience
	public function user_add_update_experience_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('job_title', 'Job Title', 'trim|required');
		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_experience_id = $this->input->post('user_experience_id');
				$experience_title = $this->input->post('job_title');
				$company_name = $this->input->post('company_name');
				$currently_working = $this->input->post('currently_working');
				$work_from = $this->input->post('work_from');
				$work_to = $this->input->post('work_to');
				$description = $this->input->post('description');
			   
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
			
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$experience_data=array(
					'user_id'=>$user_id,
					'experience_title'=>$experience_title,
					'company_name'=>$company_name,
					'currently_working'=>$currently_working,
					'start_date'=>$work_from,
					'end_date'=>$work_to,
					'experience_description'=>$description
				);
				
				$data = $this->security->xss_clean($experience_data);
				
				// code to update the user functional id
				$user_functional_data=array(
					'functional_id'=>$experience_title
				);
				$user_result = update_table('jp_users',$cond,$user_functional_data);
				// code ends here
				
				if(!empty($user_experience_id))
				{	
					$experience_condition ="user_experience_id = '$user_experience_id'";
					
					$result = update_table('jp_users_experience',$experience_condition,$data);
					if($result)
					{
						$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_success')
						],REST_Controller::HTTP_OK);
						
					}else{
							$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('update_fail')
							], REST_Controller::HTTP_OK);
					}
				}else{
						$result = insertdata('jp_users_experience',$data);
						if($result)
						{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('register_success_message')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
									'status' => true,
									'code' => 401,
									'data'=>$this->empty_object,
									'message' =>$this->lang->line('resend_email_tan_not_sent')
								], REST_Controller::HTTP_OK);
						}
				}
		}
	}
	// function ends here
	
	// function to add and update the user education
	public function user_add_update_education_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('college_university_name', 'College/University Name', 'trim|required');
		$this->form_validation->set_rules('degree_name', 'Degree Name', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_education_id = $this->input->post('user_education_id');
				$college_name = $this->input->post('college_university_name');
				$degree_level_id = $this->input->post('degree_level');
				$degree_type_id = $this->input->post('degree_name');
				$currently_pursuing = $this->input->post('currently_pursuing');
				$start_date = $this->input->post('start_date');
				$end_date = $this->input->post('end_date');
							   
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
			
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$education_data=array(
					'user_id'=>$user_id,
					'college_name'=>$college_name,
					'degree_level_id'=>$degree_level_id,
					'degree_type_id'=>$degree_type_id,
					'currently_pursuing'=>$currently_pursuing,
					'start_date'=>$start_date,
					'end_date'=>$end_date
				);
				
				$data = $this->security->xss_clean($education_data);
				
				if(!empty($user_education_id))
				{	
					$education_condition ="user_education_id = '$user_education_id'";
					
					$result = update_table('jp_users_education',$education_condition,$data);
					if($result)
					{
						$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_success')
						],REST_Controller::HTTP_OK);
						
					}else{
							$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('update_fail')
							], REST_Controller::HTTP_OK);
					}
				}else{
						$result = insertdata('jp_users_education',$data);
						if($result)
						{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('register_success_message')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
									'status' => true,
									'code' => 401,
									'data'=>$this->empty_object,
									'message' =>$this->lang->line('resend_email_tan_not_sent')
								], REST_Controller::HTTP_OK);
						}
				}
		}
	}
	// function ends here
	
	// function to add and update the user job preferences
	public function user_add_update_jobpreference_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('location_ids', 'Locations Name', 'trim|required');
		$this->form_validation->set_rules('industry_ids', 'Industry Name', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$preference_id = $this->input->post('preference_id');
				$string_location_ids = $this->input->post('location_ids');
				$location_ids = json_decode($string_location_ids);
				
				$string_industry_ids = $this->input->post('industry_ids');
				$industry_ids = json_decode($string_industry_ids);
				
				$expected_salary = $this->input->post('expected_salary');
											   
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
			
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);
				
				$comma_location_ids = implode(',',$location_ids);
				$comma_industry_ids = implode(',',$industry_ids);
				
				$preference_data=array(
					'user_id'=>$user_id,
					'location_ids'=>$comma_location_ids,
					'industry_ids'=>$comma_industry_ids,
					'expected_salary'=>$expected_salary
				);
				
				$data = $this->security->xss_clean($preference_data);
				
				if(!empty($preference_id))
				{	
					$preference_condition ="preference_id = '$preference_id'";
					
					$result = update_table('jp_users_preference',$preference_condition,$data);
					if($result)
					{
						$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_success')
						],REST_Controller::HTTP_OK);
						
					}else{
							$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('update_fail')
							], REST_Controller::HTTP_OK);
					}
				}else{
						$result = insertdata('jp_users_preference',$data);
						if($result)
						{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('register_success_message')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
									'status' => true,
									'code' => 401,
									'data'=>$this->empty_object,
									'message' =>$this->lang->line('resend_email_tan_not_sent')
								], REST_Controller::HTTP_OK);
						}
				}
		}
	}
	// function ends here	
	
	// function for add and update the user language
	public function user_add_update_languages_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('language_ids', 'Languages', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_db_languages =array();
				
				$string_language_ids = $this->input->post('language_ids');
				$language_ids =json_decode($string_language_ids);
			   
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);				
				
				$user_saved_languages = get_all_data('jp_users_languages',$cond,'*');
				
				if(empty($user_saved_languages))
				{
					foreach($language_ids as $languages)
					{
						$batch_languages[]=array
						(
							'user_id'=>$user_id,
							'language_id'=>$languages,
							'created_at'=>date('Y-m-d H:i:s')
						);						
					}
					
					$this->db->insert_batch('jp_users_languages', $batch_languages); 
						
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('register_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
						foreach($user_saved_languages as $db_languages)
						{
							if(!in_array($db_languages['language_id'], $language_ids))
							{
								row_delete('jp_users_languages','user_language_id',$db_languages['user_language_id']);								
							}else{
									$user_db_languages[]=$db_languages['language_id'];
							}
						}
						
						foreach($language_ids as $app_languages)
						{
							if(!in_array($app_languages, $user_db_languages))
							{
								$single_language=array
								(
									'user_id'=>$user_id,
									'language_id'=>$app_languages,
									'created_at'=>date('Y-m-d H:i:s')
								);
								insertdata('jp_users_languages',$single_language);
							}
						}
						
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('update_success')
					],REST_Controller::HTTP_OK);
				}
		}
	}
	
	public function user_add_update_cv_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);	
					
				$cv_id = $this->input->post('cv_id');
				
				if(!empty($_FILES['user_cv']['size']))
				{
					$config['upload_path'] = './public/images/user_docs';
					$config['allowed_types'] = 'pdf|doc|docx';
					$config['max_size'] = 2048;
					$config['max_width'] = 1024;
					$config['max_height'] = 768;
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload', $config);
					
					$orignial_file = $_FILES['user_cv']['name'];
					$cv_title = explode('.',$orignial_file);
					
					if(!$this->upload->do_upload('user_cv'))
					{
						$error = $this->upload->display_errors();
											
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$error
						],REST_Controller::HTTP_OK);
						
					}else{
						$user_cv_name = $this->upload->data('file_name');
					}
					
					$cv_data=array(
						'user_id'=>$user_id,
						'user_cv_title'=>$cv_title[0],
						'user_cv_name'=>$user_cv_name,
						'orignal_cv_name'=>$orignial_file,
						'user_cv_status'=>'Y'
					);
					
					if(!empty($cv_id))
					{
						$cv_data['updated_at']=date('Y-m-d H:i:s');
						
						$cv_condition ="user_cv_id = '$cv_id'";					
						$result = update_table('jp_users_cvs',$cv_condition,$cv_data);
						if($result)
						{
							$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('update_success')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
								'status' => true,
								'code' => 401,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('update_fail')
								], REST_Controller::HTTP_OK);
						}
						
					}else{
						$cv_data['created_at']=date('Y-m-d H:i:s');
						
						$result = insertdata('jp_users_cvs',$cv_data);
						if($result)
						{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('register_success_message')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
									'status' => true,
									'code' => 401,
									'data'=>$this->empty_object,
									'message' =>$this->lang->line('resend_email_tan_not_sent')
								], REST_Controller::HTTP_OK);
						}
					}
				}else{
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>'Uploaded CV not found'
						],REST_Controller::HTTP_OK);	
				}
		}	
	}	
	
	public function user_add_update_video_post()
	{
		//print_r($_FILES);
		//die();
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);	
					
				$video_id = $this->input->post('video_id');
				
				if(!empty($_FILES['user_video']['size']))
				{
					$config['upload_path'] = './public/images/user_docs';
					$config['allowed_types'] = 'mp4|ogg|wmv|mov';
					//$config['max_size'] = 8500;
					//$config['max_width'] = 1500;
					//$config['max_height'] = 768;
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload', $config);
					
					$orignial_file = $_FILES['user_video']['name'];
					$video_title = explode('.',$orignial_file);
					
					if(!$this->upload->do_upload('user_video'))
					{
						$error = $this->upload->display_errors();
											
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>$error
						],REST_Controller::HTTP_OK);
						
					}else{
						$user_video_name = $this->upload->data('file_name');
					}
					
					$video_data=array(
						'user_id'=>$user_id,
						'video_title'=>$video_title[0],
						'video_name'=>$user_video_name,
						'orignal_video_name'=>$orignial_file,
						'video_status'=>'Y'
					);
					
					if(!empty($video_id))
					{
						$video_data['updated_at']=date('Y-m-d H:i:s');
						
						$video_condition ="video_id = '$video_id'";					
						$result = update_table('jp_users_videos',$video_condition,$video_data);
						if($result)
						{
							$this->response([
							'status' => true,
							'code' => 200,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('update_success')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
								'status' => true,
								'code' => 401,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('update_fail')
								], REST_Controller::HTTP_OK);
						}
						
					}else{
						$video_data['created_at']=date('Y-m-d H:i:s');
						
						$result = insertdata('jp_users_videos',$video_data);
						if($result)
						{
							$this->response([
								'status' => true,
								'code' => 200,
								'data'=>$this->empty_object,
								'message' =>$this->lang->line('register_success_message')
							],REST_Controller::HTTP_OK);
							
						}else{
								$this->response([
									'status' => true,
									'code' => 401,
									'data'=>$this->empty_object,
									'message' =>$this->lang->line('resend_email_tan_not_sent')
								], REST_Controller::HTTP_OK);
						}
					}
				}else{
						$this->response([
							'status' => false,
							'code' => 400,
							'data'=>$this->empty_object,
							'message' =>'Uploaded Video not found'
						],REST_Controller::HTTP_OK);	
				}
		}	
	}
	
	// function to get the contact us details
	public function user_contact_us_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
		$this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('message', 'Message', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				$role_id = $user_data->roleId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'first_name, last_name,user_app_language');
				
				// super admin data
				
				$admin_cond="id = '1'";
				$admin_exist=exists_indb('jp_admin',$admin_cond,'firstname, lastname, to_email');
				
				// admin data ends here
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);	
					
				$email = $this->input->post('email');
				$phone_code = $this->input->post('phone_code');
				$mobile_no = $this->input->post('mobile_no');
				$message = $this->input->post('message');
				
				$name = $isexist['first_name'].' '.$isexist['last_name'];
				
				$contact_data=array(
					'contact_name'=>$name,
					'contact_email'=>$email,
					'from_id'=>$user_id,
					'to_id'=>1,
					'phone_code'=>$phone_code,
					'mobile_no'=>$mobile_no,
					'message'=>$message,
					'from_role_id'=>$role_id,
					'to_role_id'=>1,
					'created_at'=>date('Y-m-d H:i:s')
				);
				
				$result = insertdata('jp_contact_us',$contact_data);
				if($result)
				{
					$contact_mobile=$phone_code.$mobile_no;
					$admin_name = $admin_exist['firstname'].' '.$admin_exist['lastname'];
					
					$mail_data['result']=array(
						"name" => $admin_name,
						"contact_name" =>$name,
						"contact_email" =>$email,
						"contact_mobile" =>$contact_mobile,
						"message"=>$message
					);
					
					//Send Email With OTP To User
					$to=$admin_exist['to_email'];
					$subject=$this->lang->line('contact_us_subject');;
					$attachment='';
					$cc='';
					$body=$this->load->view('email_template/contact_us',$mail_data,true);
					$emailsent=sendEmail($to, $subject, $body, $attachment, $cc);
					
					$log_data=array(
						'from_id'=>$user_id,
						'to_id'=>1,
						'notification_text'=>$message,
						'notification_type'=>'contact_us',
						'role_id'=>$role_id,
						'created_at'=>date('Y-m-d h:i:s')
					);
					
					insert_log_data('jp_notifications',$log_data);
					
					$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$this->empty_object,
						'message' =>$this->lang->line('register_success_message')
					],REST_Controller::HTTP_OK);
					
				}else{
						$this->response([
							'status' => true,
							'code' => 401,
							'data'=>$this->empty_object,
							'message' =>$this->lang->line('resend_email_tan_not_sent')
						], REST_Controller::HTTP_OK);
				}				
		}
	}
	// function ends here
	
	// function to list the notifications
	public function user_notifications_list_get()
	{
		//date_default_timezone_set('Asia/Kolkata');
		$data=array();
		$user_token=$this->input->get('user_token');		
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="user_id = '$user_id'";
		$isexist=exists_indb('jp_users',$cond,'user_app_language');
		
		$user_app_langauge = $isexist['user_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$seen_data=array('is_seen'=>1);
		$seen_cond = "to_id = '$user_id' AND to_role_id=3";
		$result = update_table('jp_notifications',$seen_cond,$seen_data);
		
		$notification_data = $this->users_model->get_notifications($user_id);
		//		print_r($notification_data);
		if(!empty($notification_data))
		{
			foreach($notification_data as $notify)
			{
				if($notify['role_id']=='2')
				{
					$company_id = $notify['from_id'];
					$company_cond="company_id = '$company_id'";
					$company_isexist=exists_indb('jp_companies',$company_cond,'company_first_name,company_last_name');
					
					$time = strtotime($notify['created_at']);
					$created_at = human_timing($time);
					
					$data[]=array(
					'first_name'=>$company_isexist['company_first_name'],
					'last_name'=>$company_isexist['company_last_name'],
					'notification_text'=>$notify['notification_text'],
					'created_at'=>$created_at
					);						
				}	
			}
			
				$this->response([
						'status' => true,
						'code' => 200,
						'data'=>$data,
						'message' =>$this->lang->line('data_found')
				],REST_Controller::HTTP_OK);
			
						
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$data,
					'message' =>$this->lang->line('user_notification_no_data_found')
				], REST_Controller::HTTP_OK);
		}
		
	}
	// function ends here
	
	public function delete_work_experience_get()
	{
		$user_token=$this->input->get('user_token');		
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="user_id = '$user_id'";
		$isexist=exists_indb('jp_users',$cond,'user_app_language');
		
		$user_app_langauge = $isexist['user_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$user_experience_id = $this->input->get('user_experience_id');
		
		row_delete('jp_users_experience','user_experience_id',$user_experience_id);
		
		$this->response([
			'status' => true,
			'code' => 200,
			'data'=>$this->empty_object,
			'message' =>$this->lang->line('delete_success')
		],REST_Controller::HTTP_OK);
		
	}
	
	public function delete_user_education_get()
	{
		$user_token=$this->input->get('user_token');		
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="user_id = '$user_id'";
		$isexist=exists_indb('jp_users',$cond,'user_app_language');
		
		$user_app_langauge = $isexist['user_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$user_education_id = $this->input->get('user_education_id');
		
		row_delete('jp_users_education','user_education_id',$user_education_id);
		
		$this->response([
			'status' => true,
			'code' => 200,
			'data'=>$this->empty_object,
			'message' =>$this->lang->line('delete_success')
		],REST_Controller::HTTP_OK);
		
	}
	
	// function for user swipe the job
	public function user_job_swipe_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		$this->form_validation->set_rules('user_status', 'User Status', 'trim|required');
		$this->form_validation->set_rules('job_post_id', 'Job Post ID', 'trim|required');
		$this->form_validation->set_rules('company_id', 'Company ID', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				
				$cond="user_id = '$user_id'";
				$isexist=exists_indb('jp_users',$cond,'first_name,last_name,user_app_language');
				
				$user_app_langauge = $isexist['user_app_language'];
				$this->lang->load('authtext', $user_app_langauge);	
					
				$job_post_id = $this->input->post('job_post_id');
				$company_id = $this->input->post('company_id');
				$user_status = $this->input->post('user_status');
				$company_status = $this->input->post('company_status');
				
				if(empty($company_status))
				{
					$company_status ='NA';
				}
				
				// check and update the user swipes
					//$user_swipe_data = get_row_data('jp_users_swipes',$cond,'total_swipes');
					
					// user package info
					$user_package_data = get_row_data('jp_users_packages',$cond,'user_package_id,free_swipes,used_swipes,package_listings');
									
					$package_id =$user_package_data['user_package_id'];
					$pack_condi ="user_package_id = '$package_id'";
					
					if(!empty($user_package_data['free_swipes']))
					{	
						$today_date = date('Y-m-d');
						$chk_swipe_cond ="user_id = '$user_id' AND used_date='$today_date'";
						$user_swipe_data = get_row_data('jp_users_swipes',$chk_swipe_cond,'used_swipes');
						
						if(empty($user_swipe_data))
						{
							$data=array(
										'user_id'=>$user_id,
										'used_date'=>date('Y-m-d'),
										'total_swipes'=>$user_package_data['free_swipes'],
										'used_swipes'=>1,
										'jobs_available'=>$user_package_data['package_listings']
							);
							insertdata('jp_users_swipes',$data);
						}else{
								$existing_swipes = $user_swipe_data['used_swipes'];
								$new_swipe = $existing_swipes+1;
								
								$data=array('used_swipes'=>$new_swipe);
								update_table('jp_users_swipes',$chk_swipe_cond,$data);
						}
				
						
					}else{
							$pack_existing_swipes = $user_package_data['used_swipes'];
							$pack_new_swipe = $pack_existing_swipes+1;							
							$pack_swipe_data=array('used_swipes'=>$pack_new_swipe);
							
							update_table('jp_users_packages',$pack_condi,$pack_swipe_data);
					}
					
					/*if(empty($user_package_data['used_swipes']))
					{	
						$pack_swipe_data=array('used_swipes'=>1);
					}else{
							$pack_existing_swipes = $user_package_data['used_swipes'];
							$pack_new_swipe = $pack_existing_swipes+1;							
							$pack_swipe_data=array('used_swipes'=>$pack_new_swipe);
					}
					update_table('jp_users_packages',$pack_condi,$pack_swipe_data);*/
					// package info ends here
					
					/*if(empty($user_swipe_data))
					{
						$data=array('user_id'=>$user_id,'total_swipes'=>1);
						insertdata('jp_users_swipes',$data);
					}else{
							$existing_swipes = $user_swipe_data['total_swipes'];
							$new_swipe = $existing_swipes+1;
							
							$data=array('total_swipes'=>$new_swipe);
							update_table('jp_users_swipes',$cond,$data);
					}*/
				// check ends here
				
				// code to check if user swipe for the same job
					$post_data=array($company_id,$user_id,$job_post_id,$user_status);
					$user_swipe_data = $this->users_model->check_swipe_job($post_data);
					
					$swipe_data=array(
					   'company_id'=>$company_id,
					   'user_id'=>$user_id,
					   'user_status'=>$user_status,
					   'company_status'=>$company_status,
					   'job_id'=>$job_post_id,
					   'user_date'=>date('Y-m-d h:i:s')
					);	
					
					if(empty($user_swipe_data))
					{
						// code for swipe entry				   
						$match_id = insertdata('jp_users_company_match',$swipe_data);
						// code ends here
					}else{
							$match_id = $user_swipe_data['match_id'];
							$swipe_cond="match_id = '$match_id'";
							update_table('jp_users_company_match',$swipe_cond,$swipe_data);
					}					
				// code ends here for swipe
				
				
				$response_message =$this->lang->line('userswipe_failure_message');
				// code for sending notification to the company
				if($user_status=='A')
				{
					$response_message =$this->lang->line('userswipe_success_message');
					
					$company_device_token = getArrayValue('jp_companies','device_token,device_name','company_id',$company_id);
					
					$job_title = getSingleValue('jp_job_posts','job_title','job_post_id',$job_post_id);
					
					$this->lang->load('pushtext');
					$user_accept_swap_job = $this->lang->line('user_accept_swap_job');
					
					$employee_name = $isexist['first_name'].' '.$isexist['last_name'];
										
					$token = array(
						'USER' =>$employee_name,
						'PROFILE'=> $job_title
					);
					$pattern = '[%s]';
					foreach($token as $key=>$val){
						$varMap[sprintf($pattern,$key)] = $val;
					}
					$final_accept_job_message = strtr($user_accept_swap_job,$varMap);
					
					if($company_device_token[0]['device_token'])
					{
						$user_token = $company_device_token[0]['device_token'];
						$user_title = 'User shown interest for Job';
						
						$log_user_data=array(
							'from_id'=>1,
							'to_id'=>$company_id,
							'notification_text'=>$final_accept_job_message,
							'notification_type'=>'swapjob_push_notification',
							'role_id'=>1,
							'to_role_id'=>2
						);
						
						$payload_data=array('match_id'=>$match_id,'user_id'=>$user_id,'company_id'=>$company_id,'job_id'=>$job_post_id,'user_status'=>$user_status,'company_status'=>$company_status,'notify_category'=>'user_swap_job');
						
						
						if($company_device_token[0]['device_name']=='A')
						{
							$user_push_status = send_push($user_token,$user_title, $final_accept_job_message,$log_user_data,$payload_data);
						}
						
						if($company_device_token[0]['device_name']=='I')
						{
							$user_push_status = send_ios_push($user_token,$user_title, $final_accept_job_message,$log_user_data,$payload_data);
						}
					}					
				}
				// code for notification ends here
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$this->empty_object,
					'message' =>$response_message
				],REST_Controller::HTTP_OK);	
			
		}
	}
	// function ends here
	
	// function for common company and users acceptence
	public function common_accepted_company_listing_get()
	{
		$users_listing = array();
		$users_data='';
		
		$user_token = $this->input->get('user_token');
		$user_data = jwt_helper::decode($user_token);
		$user_id = $user_data->userId;
		
		$cond="user_id = '$user_id'";
		$isexist=exists_indb('jp_users',$cond,'user_app_language');
				
		$user_app_langauge = $isexist['user_app_language'];
		$this->lang->load('authtext', $user_app_langauge);
		
		$company_accepted_users =array();
		//$swipe_users = $this->companies_model->swipe_accepted_users($user_id);
		$swipe_condition ="user_id = '$user_id' AND user_status='A' AND company_status='A'";
		$swipe_users = get_all_data('jp_users_company_match',$swipe_condition,'*');
		if(!empty($swipe_users))
		{
			foreach($swipe_users as $swp_user)
			{
				$company_accepted_users[]=$swp_user['company_id'];	
			}
		}
		
		// code ends here for industry 
		if(!empty($company_accepted_users))
		{			
			$users_data = $this->users_model->get_accepted_company($company_accepted_users);
		}
		
		if(!empty($users_data))
		{
			foreach($users_data as $users)
			{
				/*if(!in_array($users['user_id'], $company_rejected_users))
				{*/
					$match_id= '';
					$company_match_status='';
					$user_match_status='';
					$job_id='';
				
					if(!empty($swipe_users))
					{
						foreach($swipe_users as $swp_user)
						{
							if($users['company_id']==$swp_user['company_id'])
							{
								$match_id = $swp_user['match_id'];
								$company_match_status = $swp_user['company_status'];
								$user_match_status = $swp_user['user_status'];
								$job_id = $swp_user['job_id'];
							}
						}
					}
					
					$company_logo = base_url().'public/images/company_app_no_logo.png';
					if(!empty($users['company_logo']))
					{
						$company_logo = base_url().'public/images/company_docs/'.$users['company_logo'];	
					}
					
					$company_video = '';
					if(!empty($users['company_video']))
					{
						$company_video = base_url().'public/images/company_docs/'.$users['company_video'];	
					}
					
					$country_name='';
					if(!empty($users['company_country']))
					{
						$country_name = getSingleValue('jp_countries','name','id',$users['company_country']);
					}
					
					$state_name='';
					if(!empty($users['company_state']))
					{
						$state_name = getSingleValue('jp_states','name','id',$users['company_state']);
					}
					
					$city_name='';
					if(!empty($users['company_city']))
					{
						$city_name = getSingleValue('jp_cities','name','id',$users['company_city']);
					}
					
					$industry_name='';
					if(!empty($users['industry_id']))
					{
						$industry_name = getSingleValue('jp_industries','industry_name','industry_id',$users['industry_id']);
					}
					
					$company_size='';
					if(!empty($users['company_employees']))
					{
						$company_size = $users['company_employees'];
					}
					
					$company_founded='';
					if(!empty($users['company_established']))
					{
						$company_founded = $users['company_established'];
					}
					
					$users_listing[] = array
					(
						'company_id'=>$users['company_id'],
						'first_name'=>$users['company_first_name'],
						'last_name'=>$users['company_last_name'],
						'company_name'=>$users['company_name'],
						'company_email'=>$users['company_email'],
						'company_logo'=>$company_logo,
						'company_video'=>$company_video,
						'company_address'=>$users['company_address'],
						'country'=>$country_name,
						'state'=>$state_name,
						'city'=>$city_name,
						'company_pincode'=>$users['company_pincode'],
						'phone_code'=>$users['company_phone_code'],
						'mobile'=>$users['company_phone'],
						'industry_name'=>$industry_name,
						'company_size'=>$company_size,
						'company_founded'=>$company_founded,
						'match_id'=>$match_id,
						'company_match_status'=>$company_match_status,
						'user_match_status'=>$user_match_status,
						'job_id'=>$job_id
					);
				/*}*/
			}
			
			$data=array(
				'companies_listing'=>$users_listing
			);
			
			$this->response([
				'status' => true,
				'code' => 200,
				'data'=>$data,
				'message' =>$this->lang->line('data_found')
			],REST_Controller::HTTP_OK);
			
		}else{
				$this->response([
					'status' => true,
					'code' =>401,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('no_data_found')
				], REST_Controller::HTTP_OK);
		}
	}	
	// function ends here for common	
	
	// function for user swipe the job
	public function logout_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				$role_id = $user_data->roleId;
				
				if($role_id=='3')
				{
					$user_condition ="user_id = '$user_id'";					
					$token_data=array(
						'device_token'=>''
					);
					
					$result = update_table('jp_users',$user_condition,$token_data);
				}
				
				if($role_id=='2')
				{
					$user_condition ="company_id = '$user_id'";					
					$token_data=array(
						'device_token'=>''
					);
					
					$result = update_table('jp_companies',$user_condition,$token_data);
				}
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('register_success_message')
				],REST_Controller::HTTP_OK);	
			
		}
	}
	// function ends here	
	
	// function to update the user app language
	public function app_language_update_post()
	{
		$this->form_validation->set_rules('user_token', 'User Token', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->response([
			'status' => false,
			'code' => 400,
			'data'=>$this->empty_object,
			'message' =>preg_replace('/\s+/', ' ', trim(strip_tags(validation_errors())))	 
			],REST_Controller::HTTP_OK);
		}else{
				$user_token=$this->input->post('user_token');		
				$user_data = jwt_helper::decode($user_token);
				$user_id = $user_data->userId;
				$role_id = $user_data->roleId;
				
				//echo "user id is :".$user_id.' and role id is '.$role_id;
				//die();
				
				$user_app_language = $this->input->post('user_app_language');
				
				if($role_id=='3')
				{
					$user_condition ="user_id = '$user_id'";					
					$token_data=array(
						'user_app_language'=>$user_app_language
					);
					
					$result = update_table('jp_users',$user_condition,$token_data);
					
					$cond="user_id = '$user_id'";
					$isexist=exists_indb('jp_users',$cond,'user_app_language');
					$user_app_langauge = $isexist['user_app_language'];
				}
				
				if($role_id=='2')
				{
					$user_condition ="company_id = '$user_id'";					
					$token_data=array(
						'company_app_language'=>$user_app_language
					);
					
					$result = update_table('jp_companies',$user_condition,$token_data);
					
					$cond="company_id = '$user_id'";
					$isexist=exists_indb('jp_companies',$cond,'company_app_language');
					$user_app_langauge = $isexist['company_app_language'];
				}
				
				
				$this->lang->load('authtext', $user_app_langauge);
				
				$this->response([
					'status' => true,
					'code' => 200,
					'data'=>$this->empty_object,
					'message' =>$this->lang->line('language_success')
				],REST_Controller::HTTP_OK);	
			
		}
	}
	// function ends here for language update
	
	public function test_salary_get()
	{
		echo $this->convert_number_to_words('320000.00');
	}
	
	function convert_number_to_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			100000             => 'lakh',
			10000000          => 'crore'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
			);
			return false;
		}

		if ($number < 0) {
			return $negative . $this->convert_number_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}

		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . $this->convert_number_to_words($remainder);
				}
				break;
			case $number < 100000:
				$thousands   = ((int) ($number / 1000));
				$remainder = $number % 1000;

				$thousands = $this->convert_number_to_words($thousands);

				$string .= $thousands . ' ' . $dictionary[1000];
				if ($remainder) {
					$string .= $separator . $this->convert_number_to_words($remainder);
				}
				break;
			case $number < 10000000:
				$lakhs   = ((int) ($number / 100000));
				$remainder = $number % 100000;

				$lakhs = $this->convert_number_to_words($lakhs);

				$string = $lakhs . ' ' . $dictionary[100000];
				if ($remainder) {
					$string .= $separator . $this->convert_number_to_words($remainder);
				}
				break;
			case $number < 1000000000:
				$crores   = ((int) ($number / 10000000));
				$remainder = $number % 10000000;

				$crores = $this->convert_number_to_words($crores);

				$string = $crores . ' ' . $dictionary[10000000];
				if ($remainder) {
					$string .= $separator . $this->convert_number_to_words($remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= $this->convert_number_to_words($remainder);
				}
				break;
		}

		/*if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}*/

		return $string;
	}
	
	public function	test_decode_get()
	{
		//$token='eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJjb25zdW1lcktleSI6InBpZWJvb2tpbmcjMjAxOCIsInVzZXJJZCI6IjkiLCJyb2xlSWQiOiIzIiwiaXNzdWVkQXQiOiIyMDIwLTA3LTAzVDA5OjU3OjIwKzAwMDAiLCJ0dGwiOjYzMDcyMDAwfQ.nleXJGxrIEBzdsJDe8erdl1Osgju4oMpprZzuNClUGU';
		
		$token = 'eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJjb25zdW1lcktleSI6ImpvYndpcGVyIzIwMjAiLCJ1c2VySWQiOiI1Iiwicm9sZUlkIjoiMyIsImlzc3VlZEF0IjoiMjAyMC0wOC0wNFQwNzo1MjoxMCswMDAwIiwidHRsIjo2MzA3MjAwMH0.SPnj8bDCUWVSVvvUCfagAe8EH-ghVV0H-uEUoFdOssU';
		
		//$token = 'eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJjb25zdW1lcktleSI6ImpvYndpcGVyIzIwMjAiLCJ1c2VySWQiOiI4Iiwicm9sZUlkIjoiMiIsImlzc3VlZEF0IjoiMjAyMC0wNy0zMVQwNTozNjozNyswMDAwIiwidHRsIjo2MzA3MjAwMH0.rM7SqPUfAG0OI_NF31J2nc1-Na96EGc7u-Rg_8ZRbkY';
		
		$token_decode = jwt_helper::decode($token);
		print_r($token_decode);
	}
}